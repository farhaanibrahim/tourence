package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.rumahcoding.sd.adapters.OutputAHPAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.RespondentResponse
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_output_expertise.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OutputExpertiseActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: OutputAHPAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_output_expertise)
        Log.d("ACTIVITY", "OutputExpertiseActivity")
        app_bar.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
    }
}
