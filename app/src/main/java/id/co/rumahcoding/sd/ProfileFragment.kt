package id.co.rumahcoding.sd

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.fragment_profile.view.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        val sessionManager = SessionManager(this.requireContext())

        val name = sessionManager.fetchName()
        val email = sessionManager.fetchEmail()
        val phone = sessionManager.fetchPhone()

        view.userNameText.text = name
        view.userEmailText.text = email
        view.userPhoneText.text = phone

//        view.app_bar.setOnClickListener {
//            startActivity(Intent(activity, HomeActivity::class.java))
//        }
        view.response_history_btn.setOnClickListener {
            val intent = Intent(this.requireContext(), ResponseHistoryActivity::class.java)
            startActivity(intent)
        }

        view.logoutButton.setOnClickListener{
            sessionManager.removeToken()
            val intent = Intent(this.requireContext(), HomeActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        // Inflate the layout for this fragment
        return view
    }

}
