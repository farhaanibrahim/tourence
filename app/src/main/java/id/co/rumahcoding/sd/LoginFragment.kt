package id.co.rumahcoding.sd


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.fragment_login.view.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)

        val adapterPager = MyLoginPagerAdapter(childFragmentManager)
        adapterPager.addFragment(UserLoginFragment(), "User Login")
        adapterPager.addFragment(TravelAgentLoginFragment(), "Travel Agent Login")

        view.view_pager_profile.adapter = adapterPager
        view.tab_profile.setupWithViewPager(view.view_pager_profile)

        view.app_bar_login.setOnClickListener {
            startActivity(Intent(activity, HomeActivity::class.java))
        }

        return view
    }

    class MyLoginPagerAdapter(manager: FragmentManager): FragmentPagerAdapter(manager){

        private val fragmentList : MutableList<Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }

    }

}
