package id.co.rumahcoding.sd

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.AddAttractionResponse
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.models.MyCity
import id.co.rumahcoding.sd.utils.PopupUtil
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_add_attraction.*
import kotlinx.android.synthetic.main.activity_add_attraction.app_bar
import kotlinx.android.synthetic.main.activity_select_city.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class AddAttractionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_attraction)

        app_bar.setOnClickListener {
            startActivity(Intent(this, AttractionActivity::class.java))
            finish()
        }

        val sessionManager = SessionManager(this)
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)

        val cityId = ArrayList<Int>()
        apiEndPoint.mycity(sessionManager.fetchId()).enqueue(object: Callback<List<MyCity>> {
            override fun onFailure(call: Call<List<MyCity>>, t: Throwable) {
                Toast.makeText(this@AddAttractionActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<MyCity>>, response: Response<List<MyCity>>) {
                val saveResponse = response.body()
                val item = arrayOfNulls<String>(saveResponse!!.size)
                for (i in 0 until saveResponse.size){
                    item[i] = saveResponse[i].city_name
                    cityId.add(saveResponse[i].id)
                }
                val arrayAdapter = ArrayAdapter(this@AddAttractionActivity, android.R.layout.simple_spinner_item, item)
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                city.adapter = arrayAdapter
            }

        })

        attractioSubmitBtn.setOnClickListener {
            val selectedCity = city.selectedItemPosition
            val selectedCityId = cityId[selectedCity]

            Attraction.city_id = selectedCityId
            Attraction.attraction_name = attraction_name_text.text.toString()
            Attraction.price = attraction_price.text.toString().toInt()

            checkAttraction(selectedCityId)
        }
    }

    private fun checkAttraction(cityId: Int) {
        val token = SessionManager(this).fetchAuthToken()
        val userId = SessionManager(this).fetchId()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.attractionCheck("Bearer ${token}", cityId, userId)
        call.enqueue(object: Callback<SaveResponse>{
            override fun onFailure(call: Call<SaveResponse>, t: Throwable) {
                Toast.makeText(this@AddAttractionActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<SaveResponse>, response: Response<SaveResponse>) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if (success == true){
                    val intent = Intent(this@AddAttractionActivity, InputPriceBetweenAttraction::class.java)
                    startActivity(intent)
                } else {
                    saveAttractionData(cityId)
                }
            }
        })
    }

    private fun saveAttractionData(city_id: Int){
        attractioSubmitBtn.isEnabled = false
        PopupUtil.showLoading(this, "Please wait", "Saving ...")

        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()
        val attractionName = attraction_name_text.text.toString()
        val price = attraction_price.text.toString().toInt()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.addAttraction("Bearer "+token, userId, attractionName, city_id, price, null)

        call.enqueue(object: Callback<AddAttractionResponse> {
            override fun onFailure(call: Call<AddAttractionResponse>, t: Throwable) {
                Toast.makeText(this@AddAttractionActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<AddAttractionResponse>,
                response: Response<AddAttractionResponse>
            ) {
                val responseBody = response.body()
                if (responseBody != null){
                    PopupUtil.dismissDialog()
                    showDialog()
                } else {true
                    PopupUtil.showMsg(this@AddAttractionActivity, "Client Error", PopupUtil.SHORT)
                    PopupUtil.dismissDialog()
                }
            }

        })
    }

    private fun showDialog() {
        val mAlertDialog = AlertDialog.Builder(this@AddAttractionActivity)
        mAlertDialog.setTitle("Title")
        mAlertDialog.setMessage("Successfully added attraction")
        mAlertDialog.setPositiveButton("OKE"){dialog, id->
            startActivity(Intent(this@AddAttractionActivity, AttractionActivity::class.java))
        }

        mAlertDialog.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        startActivity(Intent(this@AddAttractionActivity, AttractionActivity::class.java))
        finish()
        return true
    }

    override fun onBackPressed() {
        startActivity(Intent(this@AddAttractionActivity, AttractionActivity::class.java))
        finish()
    }
}
