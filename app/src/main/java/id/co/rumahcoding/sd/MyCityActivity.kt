package id.co.rumahcoding.sd

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import id.co.rumahcoding.sd.models.City
import id.co.rumahcoding.sd.models.MyCity
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_my_city.*
import kotlinx.android.synthetic.main.card_city_layout.view.*
import kotlinx.android.synthetic.main.input_my_city.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyCityActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: CityListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_city)

        val sessionManager = SessionManager(this)
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()

        my_city_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        my_city_list.layoutManager = layoutManager

        app_bar_my_city.setOnClickListener{
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }

        add_my_city_button.setOnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.input_my_city, null)
            val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setTitle("Add City")
            val mAlertDialog = mBuilder.show()

//            val cityId = ArrayList<Int>()
//            apiEndPoint.fetchCity().enqueue(object: Callback<List<City>>{
//                override fun onFailure(call: Call<List<City>>, t: Throwable) {
//                    Toast.makeText(this@MyCityActivity, t.message, Toast.LENGTH_LONG).show()
//                }
//
//                override fun onResponse(call: Call<List<City>>, response: Response<List<City>>) {
//                    val saveResponse = response.body()
//                    val city = arrayOfNulls<String>(saveResponse!!.size)
//                    for (i in 0 until saveResponse.size){
//                        city[i] = saveResponse[i].city_name
//                        cityId.add(saveResponse[i].id)
//                    }
//                    val arrayAdapter = ArrayAdapter(this@MyCityActivity, android.R.layout.simple_spinner_item, city)
//                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                    mDialogView.city.adapter = arrayAdapter
//                }
//
//            })

            mDialogView.submit_city.setOnClickListener {
//                val selected_city = mDialogView.city.selectedItemPosition
//                val selected_city_id = cityId[selected_city]
                val cityName = mDialogView.city.text.toString()
                Log.d("CITY_NAME", cityName)
                apiEndPoint.addCity("Bearer $token", userId.toString(), cityName)
                    .enqueue(object: Callback<SaveResponse>{
                        override fun onFailure(call: Call<SaveResponse>, t: Throwable) {
                            Toast.makeText(this@MyCityActivity, t.message, Toast.LENGTH_LONG).show()
                        }

                        override fun onResponse(
                            call: Call<SaveResponse>,
                            response: Response<SaveResponse>
                        ) {
                            val saveResponse = response.body()
                            val success = saveResponse!!.success

                            if (success == true) {
                                Snackbar.make(root_layout_my_city, saveResponse.message, Snackbar.LENGTH_LONG).show()
                                mAlertDialog.dismiss()
                                fetchCity(userId)
                            } else {
                                Snackbar.make(root_layout_my_city, saveResponse.message, Snackbar.LENGTH_LONG).show()
                                mAlertDialog.dismiss()
                            }
                        }

                    })
            }

            mDialogView.cancel_button.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

        fetchCity(userId)
    }

    fun fetchCity(user_id: Int){
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.mycity(user_id)
        call.enqueue(object: Callback<MutableList<MyCity>> {
            override fun onFailure(call: Call<MutableList<MyCity>>, t: Throwable) {
                Toast.makeText(applicationContext,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<MutableList<MyCity>>,
                response: Response<MutableList<MyCity>>
            ) {
                adapter = CityListAdapter(
                    this@MyCityActivity,
                    user_id,
                    response.body() as MutableList<MyCity>
                )
                adapter.notifyDataSetChanged()
                my_city_list.adapter = adapter
            }
        })
    }

    class CityListAdapter(private val context: Context, private val uIdTravelAgent: Int, private val myCityList: List<MyCity>): RecyclerView.Adapter<CityListAdapter.CityViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_city_layout, parent, false)

            return CityViewHolder(itemView)
        }

        override fun getItemCount(): Int {
            return myCityList.size
        }

        override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
            val currentItem = myCityList[position]

            holder.cityName.text = currentItem.city_name
            holder.itemView.setOnClickListener {
                Toast.makeText(context, "You've selected "+currentItem.city_name + " city.", Toast.LENGTH_LONG).show()

                val intent = Intent(context, AttractionListActivity::class.java)
                intent.putExtra("city_id", currentItem.id)
                intent.putExtra("city_name", currentItem.city_name)
                context.startActivity(intent)
            }
        }

        class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val cityName: TextView = itemView.city_name
        }
    }
}
