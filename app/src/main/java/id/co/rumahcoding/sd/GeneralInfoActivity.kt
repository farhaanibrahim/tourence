package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.rumahcoding.sd.adapters.GeneralInfoListAdapter
import id.co.rumahcoding.sd.models.GeneralInfo
//import id.co.rumahcoding.sd.models.GeneralInfo.Companion.stype
import id.co.rumahcoding.sd.utils.PopupUtil
import kotlinx.android.synthetic.main.activity_general_info.*
import kotlinx.android.synthetic.main.fragment_decision.recyclerView

class GeneralInfoActivity : AppCompatActivity() {
    private var generalInfos = mutableListOf<GeneralInfo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_general_info)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val uIdTravelAgent = intent.getIntExtra("uIdTravelAgent", 0)
        val expertiseValidation = intent.getBooleanExtra("expertise", false)

        title = "Select Gender And Age"

        for (i in 0 until GeneralInfo.numberOfPerson) {
            generalInfos.add(GeneralInfo("Pria", 27))
        }

        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(this@GeneralInfoActivity)
            // set the custom adapter to the RecyclerView
            adapter = GeneralInfoListAdapter(generalInfos)
        }

        startButton.setOnClickListener {
            var moreThan100 = false

            generalInfos.forEach { item ->
                if(item.age > 100) {
                    moreThan100 = true
                }
            }

            if(moreThan100) {
                PopupUtil.showMsg(this@GeneralInfoActivity, "Invalid ages (more than 100)", PopupUtil.SHORT)
                return@setOnClickListener
            }

            Log.d("GeneralInfoActivity", generalInfos.toString())
            GeneralInfo.generalInfos = generalInfos

            val intent = Intent(this@GeneralInfoActivity, MainActivity::class.java)
            intent.putExtra("uIdTravelAgent", uIdTravelAgent)
            intent.putExtra("expertise", expertiseValidation)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}