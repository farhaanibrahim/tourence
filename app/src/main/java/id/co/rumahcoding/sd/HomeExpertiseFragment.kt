package id.co.rumahcoding.sd


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.rumahcoding.sd.adapters.MyCityListExpertiseAdapter
import id.co.rumahcoding.sd.utils.SessionManager
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.ExpertiseTravelAgent
import id.co.rumahcoding.sd.models.MyCityExpertise
import kotlinx.android.synthetic.main.fragment_home_expertise.*
import kotlinx.android.synthetic.main.fragment_home_expertise.view.*
import retrofit2.Call
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class HomeExpertiseFragment : Fragment() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: MyCityListExpertiseAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home_expertise, container, false)

        val sessionManager = SessionManager(this.requireContext())
        view.user_name.text = sessionManager.fetchName()

        view.city_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this.requireContext())
        view.city_list.layoutManager = layoutManager

        fetchData()

        return view
    }

    private fun fetchData() {
        val sessionManager = SessionManager(this.requireContext())
        val token = sessionManager.fetchAuthToken()
        val userExpertise = sessionManager.fetchId()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)

        val getTravelAgent = apiEndPoint.getTravelAgent("Bearer $token", userExpertise)
        getTravelAgent.enqueue(object : retrofit2.Callback<ExpertiseTravelAgent>{
            override fun onFailure(call: Call<ExpertiseTravelAgent>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ExpertiseTravelAgent>,
                response: Response<ExpertiseTravelAgent>
            ) {
                val saveResponse = response.body()
                Log.d("SAVE RESPONSE : ", saveResponse.toString())
                val success = saveResponse!!.success
                if (success){
                    val uIdTravelAgent = saveResponse.user.id
                    travel_agent_name.text = saveResponse.user.name
                    travel_agent_email.text = saveResponse.user.email
                    travel_agent_phone.text = saveResponse.user.phone

                    val call = apiEndPoint.myCityExpertise("Bearer $token", userExpertise)
                    call.enqueue(object : retrofit2.Callback<MutableList<MyCityExpertise>>{
                        override fun onFailure(call: Call<MutableList<MyCityExpertise>>, t: Throwable) {
                            Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                        }

                        override fun onResponse(
                            call: Call<MutableList<MyCityExpertise>>,
                            response: Response<MutableList<MyCityExpertise>>
                        ) {
                            adapter = MyCityListExpertiseAdapter(this@HomeExpertiseFragment.requireContext(), uIdTravelAgent, response.body() as MutableList<MyCityExpertise>)
                            adapter.notifyDataSetChanged()
                            city_list.adapter = adapter
                        }

                    })

                } else {
                    Toast.makeText(context, "Something went wrong :/", Toast.LENGTH_LONG).show()
                }
            }

        })
    }

}
