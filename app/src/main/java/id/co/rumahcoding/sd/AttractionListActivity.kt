package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.rumahcoding.sd.adapters.AttractionListAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.AddAttractionResponse
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.utils.PopupUtil
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_add_attraction.*
import kotlinx.android.synthetic.main.activity_attraction_list.*
import kotlinx.android.synthetic.main.activity_attraction_list.app_bar
import kotlinx.android.synthetic.main.input_my_attraction.*
import kotlinx.android.synthetic.main.input_my_attraction.attraction_price
import kotlinx.android.synthetic.main.input_my_attraction.view.*
import retrofit2.*

class AttractionListActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: AttractionListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attraction_list)

        val cityId: Int = intent.getIntExtra("city_id", 0)
        val cityName = intent.getStringExtra("city_name")
        city_name.text = cityName

        app_bar.setOnClickListener {
            startActivity(Intent(this, MyCityActivity::class.java))
        }

        add_attraction_button.setOnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.input_my_attraction, null)
            val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setTitle("Add Attraction")
            val mAlertDialog = mBuilder.show()

            mDialogView.submit_attraction.setOnClickListener {
                val attractionName = mAlertDialog.attraction_name.text.toString()
                val attractionPrice = mAlertDialog.attraction_price.text.toString().toInt()

                Attraction.city_id = cityId
                Attraction.attraction_name = attractionName
                Attraction.price = attractionPrice

                checkAttraction(cityId, cityName!!, attractionName, attractionPrice)
                mAlertDialog.dismiss()
            }

            mDialogView.cancel_button.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

        attraction_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        attraction_list.layoutManager = layoutManager

        fetchDataAttraction(cityId, cityName)
    }

    private fun fetchDataAttraction(cityId: Int, cityName: String) {
        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.attractionData2("Bearer ${token}", cityId, userId)
        call.enqueue(object : Callback<MutableList<Attraction>> {
            override fun onFailure(call: Call<MutableList<Attraction>>, t: Throwable) {
                Toast.makeText(this@AttractionListActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<MutableList<Attraction>>,
                response: Response<MutableList<Attraction>>
            ) {
                adapter = AttractionListAdapter(this@AttractionListActivity, response.body() as MutableList<Attraction>, cityId, cityName)
                adapter.notifyDataSetChanged()
                attraction_list.adapter = adapter
            }

        })
    }

    private fun checkAttraction(cityId: Int, cityName: String, attractionName: String, attractionPrice: Int) {
        val token = SessionManager(this).fetchAuthToken()
        val userId = SessionManager(this).fetchId()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.attractionCheck("Bearer ${token}", cityId, userId)
        call.enqueue(object: Callback<SaveResponse>{
            override fun onFailure(call: Call<SaveResponse>, t: Throwable) {
                Toast.makeText(this@AttractionListActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<SaveResponse>, response: Response<SaveResponse>) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if (success == true){
                    val intent = Intent(this@AttractionListActivity, InputPriceBetweenAttraction::class.java)
                    intent.putExtra("city_id", cityId)
                    intent.putExtra("city_name", cityName)
                    startActivity(intent)
                } else {
                    saveAttractionData(cityId, attractionName, attractionPrice)
                }
            }
        })
    }

    private fun saveAttractionData(city_id: Int, attractionName: String, attractionPrice: Int){
        PopupUtil.showLoading(this, "Please wait", "Saving ...")

        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.addAttraction("Bearer "+token, userId, attractionName, city_id, attractionPrice, null)

        call.enqueue(object: Callback<AddAttractionResponse> {
            override fun onFailure(call: Call<AddAttractionResponse>, t: Throwable) {
                Toast.makeText(this@AttractionListActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<AddAttractionResponse>,
                response: Response<AddAttractionResponse>
            ) {
                val responseBody = response.body()
                if (responseBody != null){
                    val cityId: Int = intent.getIntExtra("city_id", 0)
                    val cityName = intent.getStringExtra("city_name")
                    fetchDataAttraction(cityId, cityName!!)
                    PopupUtil.dismissDialog()
                    showDialog()
                } else {
                    attractioSubmitBtn.isEnabled = true
                    PopupUtil.showMsg(this@AttractionListActivity, "Client Error", PopupUtil.SHORT)
                    PopupUtil.dismissDialog()
                }
            }

        })
    }

    private fun showDialog() {
        val mAlertDialog = android.app.AlertDialog.Builder(this@AttractionListActivity)
        mAlertDialog.setTitle("Title")
        mAlertDialog.setMessage("Successfully added attraction")
        mAlertDialog.setPositiveButton("OKE"){dialog, id->
            dialog.dismiss()
        }

        mAlertDialog.show()
    }
}
