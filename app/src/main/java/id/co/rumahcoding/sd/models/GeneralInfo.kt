package id.co.rumahcoding.sd.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class GeneralInfo(var gender: String, var age: Int) : Parcelable {
    companion object {
        var userid: Int = 0
        var numberOfPerson: Int = 0
        var travelingBudget: Int = 0
        var day: Int = 0
        var night: Int = 0
        var travelingStartDate: String = ""
        var travelingEndDate: String = ""
        var generalInfos = mutableListOf<GeneralInfo>()
        var city = ""
        var cityid: Int = 0
        var attractions = mutableListOf<String>()
    }
}