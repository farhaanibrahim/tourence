package id.co.rumahcoding.sd.models

data class AttractionComparison(
    var user_id: Int,
    var attraction_id: Int,
    var attraction_destination: Int,
    var price: Int,
    var distance: Int,
    var time: Int,
    var initial_attraction: String,
    var final_attraction: String
)