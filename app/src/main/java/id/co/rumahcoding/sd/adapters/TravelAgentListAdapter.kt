package id.co.rumahcoding.sd.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.*
import id.co.rumahcoding.sd.models.TravelAgent
import kotlinx.android.synthetic.main.card_travel_agent_layout.view.*

class TravelAgentListAdapter(private val context: Context, private val travelAgentList: List<TravelAgent>) : RecyclerView.Adapter<TravelAgentListAdapter.TravelAgentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TravelAgentViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_travel_agent_layout, parent, false)

        return TravelAgentViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return travelAgentList.size
    }

    override fun onBindViewHolder(holder: TravelAgentViewHolder, position: Int) {
        val currentItem = travelAgentList[position]

        holder.userName.text = currentItem.name
        holder.userPhone.text = currentItem.phone
        holder.userEmail.text = currentItem.email

        holder.itemView.setOnClickListener {
            val intent = Intent(context, SelectCityActivity::class.java)
            intent.putExtra("uIdTravelAgent", currentItem.id)
            context.startActivity(intent)
        }
    }

    class TravelAgentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val userName: TextView = itemView.user_name
        val userPhone: TextView = itemView.user_phone
        val userEmail: TextView = itemView.user_email
    }
}