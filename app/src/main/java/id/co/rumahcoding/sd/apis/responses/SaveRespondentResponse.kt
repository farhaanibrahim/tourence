package id.co.rumahcoding.sd.apis.responses

class SaveRespondentResponse (val success: Boolean, val respondentId: Int, val message : String)