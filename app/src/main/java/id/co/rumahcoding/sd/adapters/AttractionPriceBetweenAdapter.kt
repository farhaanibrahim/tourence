package id.co.rumahcoding.sd.adapters

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.Attraction
import kotlinx.android.synthetic.main.item_price_between_attraction.view.*

class AttractionPriceBetweenAdapter(private val attractionList: MutableList<Attraction>): RecyclerView.Adapter<AttractionPriceBetweenAdapter.AttractionPriceBetweenViewHolder>() {
    override fun getItemCount(): Int {
        return attractionList.size
    }

    override fun onBindViewHolder(holder: AttractionPriceBetweenViewHolder, position: Int) {
        val currentItem = attractionList[position]
        holder.attraction1.text = Attraction.attraction_name
        holder.attraction2.text = currentItem.attraction_name
        holder.priceBetween.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()){
                    currentItem.price_between = s.toString().toInt()
                }
            }

        })
        holder.distanceBetween.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()){
                    currentItem.distance_between = s.toString().toInt()
                }
            }

        })
        holder.timeBetween.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()){
                    currentItem.time_between = s.toString().toInt()
                }
            }

        })
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AttractionPriceBetweenViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_price_between_attraction, parent, false)
        return AttractionPriceBetweenViewHolder(itemView)
    }

    class AttractionPriceBetweenViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView) {

        val attraction1: TextView = itemView.attraction1
        val attraction2: TextView = itemView.attraction2
        val priceBetween: EditText = itemView.price_between_attraction
        val distanceBetween: EditText = itemView.distance_between_attraction
        val timeBetween: EditText = itemView.time_between_attraction
    }
}