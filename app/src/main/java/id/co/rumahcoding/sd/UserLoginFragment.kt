package id.co.rumahcoding.sd


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.rumahcoding.sd.apis.responses.LoginResponse
import id.co.rumahcoding.sd.apis.responses.UserDataResponse
import kotlinx.android.synthetic.main.fragment_user_login.*
import kotlinx.android.synthetic.main.fragment_user_login.view.*
import id.co.rumahcoding.sd.utils.Connectivity
import id.co.rumahcoding.sd.utils.PopupUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import id.co.rumahcoding.sd.utils.SessionManager
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint

/**
 * A simple [Fragment] subclass.
 */
class UserLoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user_login, container, false)

        view.btn_login.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()

            if(email.isBlank()) {
                view.emailEditText.error = "Email can't be empty"
                view.emailEditText.requestFocus()
                return@setOnClickListener
            }

            if(password.isBlank()) {
                view.passwordEditText.error = "Password can't be empty"
                view.passwordEditText.requestFocus()
                return@setOnClickListener
            }

            if (Connectivity.isConnected(this.requireContext())){
                loginProcess(view, email, password)
            } else {
                PopupUtil.showMsg(this.requireContext(), "Please check your internet connection", PopupUtil.SHORT)
            }
        }

        view.registerButton.setOnClickListener {
            startActivity(Intent(activity, RegisterActivity::class.java))
        }

        return view
    }

    private fun loginProcess(view: View, email: String, password: String) {
        val sessionManager = SessionManager(this.requireContext())

        view.btn_login.isEnabled = false
        PopupUtil.showLoading(this.requireContext(), null, "Please wait")

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.userLogin(email, password)

        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if(success == true){
                    val token = saveResponse.data.token

                    val callUserData = apiEndPoint.userData("Bearer "+token)
                    callUserData.enqueue(object : Callback<UserDataResponse> {
                        override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                            val intent = Intent(context, StartActivity::class.java)
                            startActivity(intent)
                        }

                        override fun onResponse(
                            call: Call<UserDataResponse>,
                            response: Response<UserDataResponse>
                        ) {
                            val responseBody = response.body()
                            val id = responseBody!!.data.user.id
                            val name = responseBody.data.user.name
                            val phone = responseBody.data.user.phone
                            val email = responseBody.data.user.email
                            val roleId = responseBody.data.user.role_id
                            val expertise = responseBody.data.user.expertise

                            sessionManager.saveAuthToken(token, id, name, phone, email, roleId, expertise.toString())

                            val intent = Intent(activity, HomeActivity::class.java)
                            startActivity(intent)
                            activity?.finish()
                        }

                    })

                } else {
                    view.btn_login?.isEnabled = true
                    PopupUtil.showMsg(activity, "Wrong Email or Password", PopupUtil.SHORT)
                    PopupUtil.dismissDialog()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                view.btn_login?.isEnabled = true
                PopupUtil.showMsg(activity, "Internal server error", PopupUtil.SHORT)
                PopupUtil.dismissDialog()
            }
        })
    }
}
