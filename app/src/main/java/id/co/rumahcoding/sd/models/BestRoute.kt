package id.co.rumahcoding.sd.models

data class BestRoute (
    val bestRoute: List<String>
)