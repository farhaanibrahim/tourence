package id.co.rumahcoding.sd.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Login(var email: String, var password: String): Parcelable
