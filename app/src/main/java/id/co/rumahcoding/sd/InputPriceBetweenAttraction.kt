package id.co.rumahcoding.sd

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.beust.klaxon.Klaxon
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.utils.SessionManager
import id.co.rumahcoding.sd.adapters.AttractionPriceBetweenAdapter
import id.co.rumahcoding.sd.apis.responses.AddAttractionResponse
import id.co.rumahcoding.sd.utils.PopupUtil
import kotlinx.android.synthetic.main.activity_input_price_between_attraction.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InputPriceBetweenAttraction : AppCompatActivity() {
    var attractionList: MutableList<Attraction> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_price_between_attraction)

        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()
        val cityId = intent.getIntExtra("city_id", 0)
        val cityName = intent.getStringExtra("city_name")!!
        Log.d("CITY NAME : ", cityName)

        recyclerViewAttractionPriceBetween.setItemViewCacheSize(10)
        recyclerViewAttractionPriceBetween.setHasFixedSize(true)

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val fetchAttraction = apiEndPoint.attractionData2("Bearer ${token}", Attraction.city_id, userId)
        fetchAttraction.enqueue(object : retrofit2.Callback<List<Attraction>> {
            override fun onFailure(call: Call<List<Attraction>>, t: Throwable) {
                Toast.makeText(this@InputPriceBetweenAttraction, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<List<Attraction>>,
                response: Response<List<Attraction>>
            ) {
//                recyclerViewAttractionPriceBetween.adapter = AttractionPriceBetweenAdapter(response.body() as MutableList<Attraction>)
                val saveResponse = response.body()
                for(element in saveResponse!!){
                    attractionList.add(Attraction(element.id, element.user_id, element.city_id, element.attraction_name, element.price, 0, 0, 0))
                }
                Log.d("ATTRACTION LIST : ", attractionList.toString())
                recyclerViewAttractionPriceBetween.apply {
                    layoutManager = LinearLayoutManager(this@InputPriceBetweenAttraction)
                    adapter = AttractionPriceBetweenAdapter(attractionList)
                }
                recyclerViewAttractionPriceBetween.adapter!!.notifyDataSetChanged()
            }
        })

        save_button.setOnClickListener {
            var emptyPrice = false
            attractionList.forEach { item ->
                if (item.price_between == 0) {
                    emptyPrice = true
                }
            }

            if (emptyPrice){
                PopupUtil.showMsg(this, "There are still empty fields", PopupUtil.SHORT)
                return@setOnClickListener
            }

            save_button.isEnabled = false
            PopupUtil.showLoading(this, "Please wait", "Saving ...")

            val attractionPriceBetween = Klaxon().toJsonString(attractionList)
            Log.d("Price Comparison: ", attractionPriceBetween)

            val saveData = apiEndPoint.addAttraction("Bearer ${token}", userId, Attraction.attraction_name, Attraction.city_id, Attraction.price, attractionPriceBetween)
            saveData.enqueue(object: Callback<AddAttractionResponse>{
                override fun onFailure(call: Call<AddAttractionResponse>, t: Throwable) {
                    save_button.isEnabled = true
                    PopupUtil.showMsg(this@InputPriceBetweenAttraction, t.message, PopupUtil.SHORT)
                    PopupUtil.dismissDialog()
                }

                override fun onResponse(
                    call: Call<AddAttractionResponse>,
                    response: Response<AddAttractionResponse>
                ) {
                    val saveResponse = response.body()
                    if (saveResponse!!.success){
                        PopupUtil.showMsg(this@InputPriceBetweenAttraction, "Data successfully saved", PopupUtil.SHORT)

                        Attraction.city_id = 0
                        Attraction.attraction_name = ""
                        Attraction.price = 0

                        val intent = Intent(this@InputPriceBetweenAttraction, AttractionListActivity::class.java)
                        intent.putExtra("city_id", cityId)
                        intent.putExtra("city_name", cityName)
                        startActivity(intent)
                        finish()
                    } else {
                        PopupUtil.showMsg(this@InputPriceBetweenAttraction, "Save data failed", PopupUtil.LONG)
                    }
                }

            })
        }
    }

    override fun onBackPressed() {
        val cityId = intent.getIntExtra("city_id", 0)
        val cityName = intent.getStringExtra("city_name")!!
        val builder = AlertDialog.Builder(this@InputPriceBetweenAttraction)
            .setTitle("Are you sure?")
            .setMessage("If you back, your work will be lost, are you sure?")
            .setPositiveButton("Yes", DialogInterface.OnClickListener{
                    _, _ ->
                startActivity(Intent(this@InputPriceBetweenAttraction, AttractionListActivity::class.java))
                intent.putExtra("city_id", cityId)
                intent.putExtra("city_name", cityName)
                finish()
            })
            .setNegativeButton("No", DialogInterface.OnClickListener{
                    dialog, _ -> dialog.dismiss()
            })
            .create()
        builder.show()
    }
}
