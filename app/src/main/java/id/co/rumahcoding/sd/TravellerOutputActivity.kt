package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.activity_traveller_output.*

class TravellerOutputActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_traveller_output)

        app_bar.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }

        val adapterPager = OutputTravellerPagerAdapter(supportFragmentManager)
        adapterPager.addFragment(OutputAHPTravellerFragment(), "Ahp")
        adapterPager.addFragment(OutputItineraryTravellerFragment(), "Itinerary")

        view_pager_output_traveller.adapter = adapterPager
        tab_output_traveller.setupWithViewPager(view_pager_output_traveller)
    }

    class OutputTravellerPagerAdapter(manager: FragmentManager): FragmentPagerAdapter(manager){

        private val fragmentList : MutableList<Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }
    }
}
