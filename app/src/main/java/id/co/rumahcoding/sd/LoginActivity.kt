package id.co.rumahcoding.sd

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import id.co.rumahcoding.sd.models.GeneralInfo
import id.co.rumahcoding.sd.utils.PopupUtil
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception
import java.util.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val actionbar = supportActionBar
        actionbar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#03A9F4")))
        actionbar?.setDisplayHomeAsUpEnabled(true)

        val uIdTravelAgent = intent.getIntExtra("uIdTravelAgent", 0)
        val expertiseValidation = intent.getBooleanExtra("expertise", false)

        city.text = GeneralInfo.city

        app_bar_login.setOnClickListener {
            val intent = Intent(this@LoginActivity, SelectCityActivity::class.java)
            intent.putExtra("uIdTravelAgent", uIdTravelAgent)
            startActivity(intent)
        }

        btn_login.setOnClickListener {
            //GeneralInfo.stype = stype

            var numberOfPerson = 1
            var day = 1
            var night = 1
            var travelingBudget = 100000
            val travelingStartDate = travelingStartDateEditText.text.toString()
            val travelingEndDate = travelingEndDateEditText.text.toString()

            //GeneralInfo.name = et_name.text.toString()
            //GeneralInfo.phone = et_phone.text.toString()

            try {
                numberOfPerson = numberOfPersonEditText.text.toString().toInt()
            }
            catch (e: Exception) {
                numberOfPersonEditText.error = "Invalid number"
                numberOfPersonEditText.requestFocus()
                return@setOnClickListener
            }

            if(numberOfPerson < 1) {
                numberOfPersonEditText.error = "Person at least 1"
                numberOfPersonEditText.requestFocus()
                return@setOnClickListener
            }

            try {
                travelingBudget = travelingBudgetEditText.text.toString().toInt()
            }
            catch (e: Exception) {
                travelingBudgetEditText.error = "Invalid number"
                travelingBudgetEditText.requestFocus()
                return@setOnClickListener
            }

            if(travelingBudget <= 0) {
                travelingBudgetEditText.error = "Invalid budget"
                travelingBudgetEditText.requestFocus()
                return@setOnClickListener
            }

            try {
                day = dayEditText.text.toString().toInt()
            }
            catch (e: Exception) {
                dayEditText.error = "Invalid number"
                dayEditText.requestFocus()
                return@setOnClickListener
            }

            if(day < 1) {
                dayEditText.error = "At least 1 day"
                dayEditText.requestFocus()
                return@setOnClickListener
            }

            try {
                night = nightEditText.text.toString().toInt()
            }
            catch (e: Exception) {
                nightEditText.error = "Invalid number"
                nightEditText.requestFocus()
                return@setOnClickListener
            }

            if(night < 1) {
                nightEditText.error = "At least 1 night"
                nightEditText.requestFocus()
                return@setOnClickListener
            }

            if(travelingStartDate.isBlank()) {
                PopupUtil.showMsg(baseContext, "Start Date can't be empty", PopupUtil.SHORT)
                return@setOnClickListener
            }

            if(travelingEndDate.isBlank()) {
                PopupUtil.showMsg(baseContext, "End Date can't be empty", PopupUtil.SHORT)
                return@setOnClickListener
            }

            GeneralInfo.numberOfPerson = numberOfPerson
            GeneralInfo.day = day
            GeneralInfo.night = night
            GeneralInfo.travelingBudget = travelingBudget
            GeneralInfo.travelingStartDate = travelingStartDate
            GeneralInfo.travelingEndDate = travelingEndDate

            GeneralInfo.generalInfos.clear()

            val intent = Intent(this@LoginActivity, GeneralInfoActivity::class.java)
            intent.putExtra("uIdTravelAgent", uIdTravelAgent)
            intent.putExtra("expertise", expertiseValidation)
            startActivity(intent)
        }

        travelingStartDateEditText.setOnClickListener {
            openDatePicker(1)
        }

        travelingEndDateEditText.setOnClickListener {
            openDatePicker(2)
        }
    }

    fun openDatePicker(type: Int) {
        val datePickerDialog = DatePickerDialog()
        val calendar = Calendar.getInstance()

        if(type == 1) {
            datePickerDialog.minDate = calendar
        }
        else if(type == 2) {
            val startDate = travelingStartDateEditText.text.toString()

            if(startDate.isBlank()) {
                datePickerDialog.minDate = calendar
            }
            else {
                val startDates = startDate.split("-")
                val calendar2 = Calendar.getInstance()

                try {
                    calendar2.set(Calendar.YEAR, startDates[0].toInt())
                    calendar2.set(Calendar.MONTH, startDates[1].toInt()-1)
                    calendar2.set(Calendar.DAY_OF_MONTH, startDates[2].toInt())
                    datePickerDialog.minDate = calendar2
                }
                catch (e: Exception) {
                    datePickerDialog.minDate = calendar2
                }
            }
        }

        datePickerDialog.onDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            when(type) {
                1 -> travelingStartDateEditText.setText("$year-${monthOfYear+1}-$dayOfMonth")
                2 -> travelingEndDateEditText.setText("$year-${monthOfYear+1}-$dayOfMonth")
            }
        }

        datePickerDialog.show(supportFragmentManager, "Datepickerdialog")
        /*val datePickerDialog = DatePickerDialog(this@LoginActivity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    when(type) {
                        1 -> travelingStartDateEditText.setText("$year-$monthOfYear-$dayOfMonth")
                        2 -> travelingEndDateEditText.setText("$year-$monthOfYear-$dayOfMonth")
                    }
        }, year, month, day)*/
    }

    override fun onSupportNavigateUp(): Boolean {
        startActivity(Intent(this@LoginActivity, SelectCityActivity::class.java))
        return true
    }
}
