package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.rumahcoding.sd.adapters.AttractionComparisonListAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.AttractionComparison
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_attraction_comparison.*
import retrofit2.Call
import retrofit2.Response

class AttractionComparisonActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: AttractionComparisonListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attraction_comparison)

        app_bar.setOnClickListener {
            startActivity(
                Intent(this, AttractionListActivity::class.java)
                    .putExtra(
                        "city_id", intent.getIntExtra("city_id", 0))
                    .putExtra("city_name", intent.getStringExtra("city_name"))
            )
        }

        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()
        val initAttractionId = intent.getIntExtra("initial_attraction_id", 0)
        Log.d("INIT ATTRACTION ID : ", initAttractionId.toString())

        initial_attraction.text = intent.getStringExtra("initial_attraction")
        attraction_comparison_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        attraction_comparison_list.layoutManager = layoutManager

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.attractionComparison("Bearer ${token}", userId, initAttractionId)

        call.enqueue(object : retrofit2.Callback<MutableList<AttractionComparison>> {
            override fun onFailure(call: Call<MutableList<AttractionComparison>>, t: Throwable) {
                Toast.makeText(applicationContext,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<MutableList<AttractionComparison>>,
                response: Response<MutableList<AttractionComparison>>
            ) {
                adapter = AttractionComparisonListAdapter(this@AttractionComparisonActivity, response.body() as MutableList<AttractionComparison>)
                adapter.notifyDataSetChanged()
                attraction_comparison_list.adapter = adapter
            }

        })
    }
}
