package id.co.rumahcoding.sd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.MyCity
import kotlinx.android.synthetic.main.item_checklist_city.view.*

class ChecklistCityAdapter(private val cityList: MutableList<MyCity>, private var selectedCity: MutableList<MyCity>): RecyclerView.Adapter<ChecklistCityAdapter.ChecklistCityViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChecklistCityViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_checklist_city, parent, false)
        return ChecklistCityViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    override fun onBindViewHolder(holder: ChecklistCityViewHolder, position: Int) {
        val currentItem = cityList[position]
        holder.checkboxCity.text = currentItem.city_name
        holder.checkboxCity.setOnClickListener {
            selectedCity.add(MyCity(currentItem.id, currentItem.city_name))
        }
    }

    class ChecklistCityViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val checkboxCity: TextView = itemView.checkbox_city
    }
}