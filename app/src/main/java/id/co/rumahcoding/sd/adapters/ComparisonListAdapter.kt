package id.co.rumahcoding.sd.adapters

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.Comparison

class ComparisonListAdapter(private val items: List<Comparison>): RecyclerView.Adapter<ComparisonListAdapter.ComparisonViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ComparisonViewHolder, position: Int) {
        val comparison: Comparison = items[position]
        holder.bind(comparison)

        holder.mContainer?.setOnClickListener(View.OnClickListener {
            comparison.read = true
            holder.mContainer?.setBackgroundColor(Color.parseColor("#C3F0B1"))
        })

        holder.mSeekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

            override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
                if(!position.equals(RecyclerView::NO_POSITION)) {
                    if(seekbar!!.isShown && fromUser) {
                        comparison.score = progress.toFloat()
                        comparison.read = true
                        holder.mContainer?.setBackgroundColor(Color.parseColor("#C3F0B1"))
                        Log.d("ComparisonListAdapter", "Change value to ${comparison.score}")
                    }
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComparisonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ComparisonViewHolder(inflater, parent)
    }

    class ComparisonViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_comparison, parent, false)) {

        var mContainer: LinearLayout? = null
        var mLeftTextView: TextView? = null
        var mRigthTextView: TextView? = null
        var mSeekBar: SeekBar? = null

        init {
            mContainer = itemView.findViewById(R.id.container)
            mLeftTextView = itemView.findViewById(R.id.leftTextView)
            mRigthTextView = itemView.findViewById(R.id.rightTextView)
            mSeekBar = itemView.findViewById(R.id.seekBar)
        }

        fun bind(comparison: Comparison) {
            if(comparison.read) {
                mContainer?.setBackgroundColor(Color.parseColor("#C3F0B1"))
            }
            else {
                mContainer?.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }

            mLeftTextView?.text = comparison.left
            mRigthTextView?.text = comparison.right
            mSeekBar?.progress = comparison.score.toInt()

            Log.d("ComparisonListAdapter", "$comparison")
        }
    }
}