package id.co.rumahcoding.sd.models

import com.google.gson.annotations.SerializedName

class City(
    @SerializedName("id") val id : Int,
    @SerializedName("user_id") val user_id: Int,
    @SerializedName("city_name") val city_name : String
)