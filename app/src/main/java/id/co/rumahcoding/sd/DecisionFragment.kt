package id.co.rumahcoding.sd

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.beust.klaxon.Klaxon
import com.marcinmoskala.math.combinations
import id.co.rumahcoding.sd.models.Decision
import kotlinx.android.synthetic.main.fragment_decision.*
import id.co.rumahcoding.sd.adapters.ComparisonListAdapter
import id.co.rumahcoding.sd.models.Comparison
import id.co.rumahcoding.sd.utils.ListUtils


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_DECISION = "decision"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DecisionFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DecisionFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DecisionFragment : Fragment() {
    // TODO: Rename and change types of parameters
    public var decision: Decision? = null
    private var listener: OnFragmentInteractionListener? = null
    private var comparisons: List<Comparison> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            decision = it.getParcelable(ARG_DECISION)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etTitle.text = decision?.root
        val childSet = decision!!.children
        val combinations = ListUtils.combinations(childSet)//childSet.combinations(2)

        if(decision?.comparisons.isNullOrEmpty()) {
            comparisons = buildComparisons2(combinations)//.sortedWith(compareBy({ it.left }, { it.right }))
            decision?.comparisons = comparisons
        }
        else {
            comparisons = decision!!.comparisons
        }


        // RecyclerView node initialized here
        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = ComparisonListAdapter(comparisons)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_decision, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(decision: Decision?) =
            DecisionFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_DECISION, decision)
                }
            }
    }

    fun <T> buildComparisons(combinations: Set<Set<T>>): List<Comparison> {
        var comparisons = mutableListOf<Comparison>()

        combinations.forEach { combination ->
            val left = combination.elementAt(0).toString()
            val right = combination.elementAt(1).toString()
            val comparison = Comparison(left, right)
            comparisons.add(comparison)

            //Log.d("DecisionFragment", "$left, $right")
            //Log.d("DecisionFragment", combination.toString())
        }

        val size = comparisons.size
        //Log.d("DecisionFragment", "$size")
        //Log.d("DecisionFragment", comparisons.toString())

        return comparisons
    }

    fun buildComparisons2(combinations: List<List<String>>): List<Comparison> {
        var comparisons = mutableListOf<Comparison>()

        combinations.forEach { combination ->
            val left = combination.elementAt(0).toString()
            val right = combination.elementAt(1).toString()
            val comparison = Comparison(left, right)
            comparisons.add(comparison)

            //Log.d("DecisionFragment", "$left, $right")
            //Log.d("DecisionFragment", combination.toString())
        }

        val size = comparisons.size
        //Log.d("DecisionFragment", "$size")
        //Log.d("DecisionFragment", comparisons.toString())

        return comparisons
    }
}
