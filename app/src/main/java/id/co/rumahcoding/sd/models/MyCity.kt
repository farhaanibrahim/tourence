package id.co.rumahcoding.sd.models

import com.google.gson.annotations.SerializedName

class MyCity (
    @SerializedName("id") val id : Int,
    @SerializedName("city_name") val city_name: String
)