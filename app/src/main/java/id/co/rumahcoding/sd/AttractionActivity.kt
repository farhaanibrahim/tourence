package id.co.rumahcoding.sd

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.MyCity
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_attraction.*
import kotlinx.android.synthetic.main.activity_attraction_list.*
import kotlinx.android.synthetic.main.activity_select_city.app_bar
import kotlinx.android.synthetic.main.activity_select_city.city_list
import kotlinx.android.synthetic.main.card_city_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AttractionActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: CityListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attraction)

        val sessionManager = SessionManager(this)
        val userId = sessionManager.fetchId()

        app_bar.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        add_attraction_button.setOnClickListener{
            startActivity(Intent(this, AddAttractionActivity::class.java))
        }

        city_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        city_list.layoutManager = layoutManager

        fetchCity(userId)
    }

    private fun fetchCity(user_id: Int){
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.mycity(user_id)
        call.enqueue(object: Callback<MutableList<MyCity>> {
            override fun onFailure(call: Call<MutableList<MyCity>>, t: Throwable) {
                Toast.makeText(applicationContext,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<MutableList<MyCity>>,
                response: Response<MutableList<MyCity>>
            ) {
                adapter = CityListAdapter(this@AttractionActivity, user_id, response.body() as MutableList<MyCity>)
                adapter.notifyDataSetChanged()
                city_list.adapter = adapter
            }
        })
    }

    class CityListAdapter(private val context: Context, private val uIdTravelAgent: Int, private val myCityList: List<MyCity>): RecyclerView.Adapter<CityListAdapter.CityViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_city_layout, parent, false)

            return CityViewHolder(itemView)
        }

        override fun getItemCount(): Int {
            return myCityList.size
        }

        override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
            val currentItem = myCityList[position]

            holder.cityName.text = currentItem.city_name
            holder.itemView.setOnClickListener {
                Toast.makeText(context, "You've selected "+currentItem.city_name + " city.", Toast.LENGTH_LONG).show()

                val intent = Intent(context, AttractionListActivity::class.java)
                intent.putExtra("city_id", currentItem.id)
                intent.putExtra("city_name", currentItem.city_name)
                context.startActivity(intent)
            }
        }

        class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val cityName: TextView = itemView.city_name
        }
    }
}
