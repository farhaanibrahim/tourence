package id.co.rumahcoding.sd

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.UserDataResponse
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.home -> {
                val sessionManager = SessionManager(this)
                val token = sessionManager.fetchAuthToken()

                val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
                val call = apiEndPoint.userData("Bearer ${token}")
                call.enqueue(object: Callback<UserDataResponse> {
                    override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                        Snackbar.make(fragmentContainer, t.message.toString(), Snackbar.LENGTH_INDEFINITE).show()
                    }

                    override fun onResponse(
                        call: Call<UserDataResponse>,
                        response: Response<UserDataResponse>
                    ) {
                        val saveResponse = response.body()
                        val success = saveResponse?.success

                        if (success == true) {
                            if (saveResponse.data.user.role_id == "2") {
                                replaceFragment(HomeTravelAgentFragment())
                            } else {
                                if (saveResponse.data.user.expertise == 1) {
                                    replaceFragment(HomeExpertiseFragment())
                                } else {
                                    replaceFragment(HomeFragment())
                                }
                            }
                        } else {
                            SessionManager(this@HomeActivity).removeToken()
                            replaceFragment(HomeFragment())
                        }
                    }

                })
                return@OnNavigationItemSelectedListener true
            }
            R.id.profile -> {
                val sessionManager = SessionManager(this)
                val token = sessionManager.fetchAuthToken()

                val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
                val call = apiEndPoint.userData("Bearer ${token}")
                call.enqueue(object: Callback<UserDataResponse> {
                    override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                        Snackbar.make(fragmentContainer, t.message.toString(), Snackbar.LENGTH_INDEFINITE).show()
                    }

                    override fun onResponse(
                        call: Call<UserDataResponse>,
                        response: Response<UserDataResponse>
                    ) {
                        val saveResponse = response.body()
                        val success = saveResponse?.success

                        if (success == true) {
                            replaceFragment(ProfileFragment())
                        } else {
                            replaceFragment(LoginFragment())
                        }
                    }

                })

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.userData("Bearer ${token}")
        call.enqueue(object: Callback<UserDataResponse> {
            override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                Snackbar.make(fragmentContainer, t.message.toString(), Snackbar.LENGTH_INDEFINITE).show()
            }

            override fun onResponse(
                call: Call<UserDataResponse>,
                response: Response<UserDataResponse>
            ) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if (success == true) {
                    if (saveResponse.data.user.role_id == "2") {
                        replaceFragment(HomeTravelAgentFragment())
                    } else {
                        if (saveResponse.data.user.expertise == 1) {
                            replaceFragment(HomeExpertiseFragment())
                        } else {
                            replaceFragment(HomeFragment())
                        }
                    }
                } else {
                    SessionManager(this@HomeActivity).removeToken()
                    replaceFragment(HomeFragment())
                }
            }

        })
    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
    }
}
