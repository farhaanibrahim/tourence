package id.co.rumahcoding.sd


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.fragment_home_travel_agent.view.*

/**
 * A simple [Fragment] subclass.
 */
class HomeTravelAgentFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_home_travel_agent, container, false)

        val sessionManager = SessionManager(this.requireContext())

        view.user_name.setText(sessionManager.fetchName())

        view.my_expertise_button.setOnClickListener {
            startActivity(Intent(activity, ExpertiseActivity::class.java))
        }

//        view.my_attraction_button.setOnClickListener {
//            startActivity(Intent(activity, AttractionActivity::class.java))
//        }

        view.my_city_button.setOnClickListener {
            startActivity(Intent(activity, MyCityActivity::class.java))
        }

        return view
    }


}
