package id.co.rumahcoding.sd.apis.responses

import id.co.rumahcoding.sd.models.Attraction

class RespondentResponse (
    val success: Boolean,
    val message: String,
    val respondent: RespondentData
)

class RespondentData(
    val id: Int,
    val user_id: Int,
    val uid_travel_agent: Int,
    val nubmber_of_person: Int,
    val travelling_budget: Int,
    val day: Int,
    val night: Int,
    val start_date: String,
    val end_date: String,
    val city_id: Int,
    val attractions: List<Attraction>
)