package id.co.rumahcoding.sd.models

data class ResponseHistory (
    val success: Boolean,
    val message: String,
    val respondent: List<Respondent>
)

data class Respondent (
    val id: Long,
    val user_id: Long,
    val uid_travel_agent: Long,
    val number_of_person: Long,
    val traveling_budget: Long,
    val day: Long,
    val night: Long,
    val start_date: String,
    val end_date: String,
    val city_id: Long,
    val created_at: String,
    val updated_at: String,
    val city: City
)