package id.co.rumahcoding.sd.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.AttractionComparisonActivity
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.StartActivity
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.models.AttractionComparison
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.card_attraction_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class AttractionListAdapter(private val context: Context, private val attractionList: List<Attraction>, private val city_id: Int, private val city_name: String) : RecyclerView.Adapter<AttractionListAdapter.AttractionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttractionViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_attraction_layout, parent, false)

        return AttractionViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return attractionList.size
    }

    override fun onBindViewHolder(holder: AttractionViewHolder, position: Int) {
        val currentItem = attractionList[position]
        holder.attractionName.text = currentItem.attraction_name
        holder.attractionPrice.text = currentItem.price.toString()
        holder.itemView.setOnClickListener {
            val sessionManager = SessionManager(context)
            val token = sessionManager.fetchAuthToken()
            val userId = sessionManager.fetchId()
            val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
            val call = apiEndPoint.attractionComparison("Bearer ${token}", userId, currentItem.id)

            call.enqueue(object: Callback<MutableList<AttractionComparison>> {
                override fun onFailure(
                    call: Call<MutableList<AttractionComparison>>,
                    t: Throwable
                ) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<MutableList<AttractionComparison>>,
                    response: Response<MutableList<AttractionComparison>>
                ) {
                    val saveResponse = response.body()
                    if (saveResponse == null) {
                        Toast.makeText(context, "Prices between attractions have not been inputted.", Toast.LENGTH_LONG).show()
                    } else {
                        Log.d("Initial Attraction : ", currentItem.id.toString())
                        val intent = Intent(context, AttractionComparisonActivity::class.java)
                        intent.putExtra("initial_attraction_id", currentItem.id)
                        intent.putExtra("initial_attraction", currentItem.attraction_name)
                        intent.putExtra("city_id", city_id)
                        intent.putExtra("city_name", city_name)
                        context.startActivity(intent)
                    }
                }

            })
        }
    }

    class AttractionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val attractionName: TextView = itemView.attraction_name
        val attractionPrice: TextView = itemView.attraction_price
    }

}