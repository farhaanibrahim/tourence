package id.co.rumahcoding.sd.models

data class ItineraryOutput (
    val success: Boolean,
    val message: String,
    val data: Data
)

data class Data (
    val id: Long,
    val respondentID: Long,
    val max: String,
    val createdAt: String,
    val updatedAt: String,
    val attractions: List<AttractionList>
)

data class AttractionList (
    val id: Long,
    val itineraryID: Long,
    val attraction: String,
    val createdAt: String,
    val updatedAt: String
)