package id.co.rumahcoding.sd.adapters

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.Comparison
import id.co.rumahcoding.sd.models.GeneralInfo

class GeneralInfoListAdapter(private val items: List<GeneralInfo>): RecyclerView.Adapter<GeneralInfoListAdapter.GeneralInfoViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: GeneralInfoViewHolder, position: Int) {
        val generalInfo: GeneralInfo = items[position]
        holder.bind(generalInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeneralInfoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return GeneralInfoViewHolder(inflater, parent)
    }

    class GeneralInfoViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_general_info, parent, false)) {

        private var mGenderSpinner: Spinner? = null
        private var mAgeEditText: EditText? = null

        init {
            mGenderSpinner = itemView.findViewById(R.id.genderSpinner)
            mAgeEditText = itemView.findViewById(R.id.ageEditText)
        }

        fun bind(generalInfo: GeneralInfo) {
            mGenderSpinner?.setSelection((mGenderSpinner?.adapter as ArrayAdapter<String>).getPosition(generalInfo.gender))
            mAgeEditText?.setText(generalInfo.age.toString())

            generalInfo.gender = mGenderSpinner?.selectedItem as String
            generalInfo.age = mAgeEditText?.text.toString().toInt()

            mGenderSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    when(p2) {
                        0 -> generalInfo.gender = "Pria"
                        1 -> generalInfo.gender = "Wanita"
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }
            }

            mAgeEditText?.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if(p0!!.isNotEmpty()) {
                        generalInfo.age = p0.toString().toInt()
                    }
                }
            })
        }
    }
}