package id.co.rumahcoding.sd.models

import com.google.gson.annotations.SerializedName

class Role(
    @SerializedName("id") val id: Int,
    @SerializedName("role_name") val role_name: String
)