package id.co.rumahcoding.sd.models

import com.google.gson.annotations.SerializedName

class TravelAgent (
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("phone") val phone : String,
    @SerializedName("email") val email : String,
    @SerializedName("role_id") val role_id : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)