package id.co.rumahcoding.sd

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.UserDataResponse
import id.co.rumahcoding.sd.utils.PopupUtil
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_choose_type.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChooseTypeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_type)

        val actionbar = supportActionBar
        actionbar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#03A9F4")))
        actionbar?.setDisplayHomeAsUpEnabled(true)

        val uIdTravelAgent: Int = intent.getIntExtra("uIdTravelAgent", 0)
        Log.d("uIdTravelAgent", uIdTravelAgent.toString())
        app_bar_choose_type.setOnClickListener {
            val intent = Intent(this@ChooseTypeActivity, SelectCityActivity::class.java)
            intent.putExtra("uIdTravelAgent", uIdTravelAgent)
            startActivity(intent)
        }

        traveller_button.setOnClickListener {
            loginSessionCheck(false)
        }

        expertise_button.setOnClickListener {
//            expertiseValidation(uIdTravelAgent)
            loginSessionCheck(true)
        }
    }

    private fun loginSessionCheck(stype:Boolean) {
        val sessionManager = SessionManager(this)
        val userToken = sessionManager.fetchAuthToken()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.userData("Bearer ${userToken}")
        call.enqueue(object: Callback<UserDataResponse>{
            override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                Snackbar.make(root_layout, t.message.toString(), Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<UserDataResponse>,
                response: Response<UserDataResponse>
            ) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if (success == true) {
                    val uIdTravelAgent: Int = intent.getIntExtra("uIdTravelAgent", 0)

                    if (stype == true) {
                        expertiseValidation(uIdTravelAgent)
                    } else {
                        val intent = Intent(this@ChooseTypeActivity, LoginActivity::class.java)
                        intent.putExtra("uIdTravelAgent", uIdTravelAgent)
                        intent.putExtra("expertise", stype)

                        startActivity(intent)
                    }
                } else {
                    val snackbar = Snackbar.make(root_layout, "You are not registered", Snackbar.LENGTH_LONG)
                    snackbar.setAction("Action" ,null)
                    snackbar.show()
                }
            }

        })
    }

    fun expertiseValidation(uIdTravelAgent: Int) {
        val sessionManager = SessionManager(this)
        val expertise = sessionManager.fetchExpertise()

        if (expertise == "1"){
            val intent = Intent(this@ChooseTypeActivity, MainActivity::class.java)
            intent.putExtra("uIdTravelAgent", uIdTravelAgent)
            intent.putExtra("expertise", true)

            startActivity(intent)
        } else {
            PopupUtil.showMsg(this@ChooseTypeActivity, "You are not assigned as expertise user", PopupUtil.SHORT)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        startActivity(Intent(this@ChooseTypeActivity, SelectCityActivity::class.java))
        return true
    }
}
