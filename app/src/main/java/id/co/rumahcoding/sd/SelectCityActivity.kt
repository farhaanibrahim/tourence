package id.co.rumahcoding.sd

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.adapters.MyCityListAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.City
import id.co.rumahcoding.sd.models.MyCity
import kotlinx.android.synthetic.main.activity_choose_type.*
import kotlinx.android.synthetic.main.activity_select_city.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SelectCityActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: MyCityListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_city)

        app_bar.setOnClickListener {
            startActivity(Intent(this@SelectCityActivity, HomeActivity::class.java))
        }

        city_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        city_list.layoutManager = layoutManager

        fetchCity()
    }

    private fun fetchCity() {
        val uIdTravelAgent = intent.getIntExtra("uIdTravelAgent", 0)

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.mycity(uIdTravelAgent)
        call.enqueue(object: Callback<MutableList<MyCity>>{
            override fun onFailure(call: Call<MutableList<MyCity>>, t: Throwable) {
                Toast.makeText(applicationContext,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<MutableList<MyCity>>,
                response: Response<MutableList<MyCity>>
            ) {
                if (response.body().isNullOrEmpty()){
                    val snackbar = Snackbar.make(root_layout_select_city, "This travel agent doesn't have a city", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Action" ,null)
                    snackbar.show()
                }

                adapter = MyCityListAdapter(this@SelectCityActivity, uIdTravelAgent, response.body() as MutableList<MyCity>)
                adapter.notifyDataSetChanged()
                city_list.adapter = adapter
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        startActivity(Intent(this@SelectCityActivity, HomeActivity::class.java))
        return true
    }
}
