package id.co.rumahcoding.sd.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.OutputExpertiseActivity
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.TravellerOutputActivity
import id.co.rumahcoding.sd.models.Respondent
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.card_response_history.view.*

class ResponseHistoryListAdapter(private val context: Context, private val responseList: List<Respondent>) : RecyclerView.Adapter<ResponseHistoryListAdapter.ResponseHistoryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResponseHistoryViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_response_history, parent, false)

        return ResponseHistoryViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return responseList.size
    }

    override fun onBindViewHolder(holder: ResponseHistoryViewHolder, position: Int) {
        val currentItem = responseList[position]
        val sessionManager = SessionManager(context)
        val expertise = sessionManager.fetchExpertise()

        holder.cityName.text = currentItem.city.city_name
        holder.budget.text = currentItem.traveling_budget.toString()
        holder.numberOfPerson.text = currentItem.number_of_person.toString()
        holder.duration.text = currentItem.day.toString()+" day(s) - "+currentItem.night.toString()+" night(s) "
        holder.itemView.setOnClickListener {
            if (expertise == "1") {
                val intent = Intent(context, OutputExpertiseActivity::class.java)
                intent.putExtra("userId", currentItem.user_id.toString())
                intent.putExtra("respondentId", currentItem.id.toString())
                intent.putExtra("cityId", currentItem.city_id.toString())
                intent.putExtra("uidTravelAgent", currentItem.uid_travel_agent.toString())
                context.startActivity(intent)
            } else {
                val intent = Intent(context, TravellerOutputActivity::class.java)
                intent.putExtra("respondentId", currentItem.id.toString())
                context.startActivity(intent)
            }
        }
    }

    class ResponseHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cityName: TextView = itemView.city_name
        val budget: TextView = itemView.budget
        val numberOfPerson: TextView = itemView.number_of_person
        val duration: TextView = itemView.duration
    }

}