package id.co.rumahcoding.sd


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.beust.klaxon.Klaxon
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.google.gson.JsonObject
import id.co.rumahcoding.sd.adapters.OutputAHPAdapter
import id.co.rumahcoding.sd.utils.SessionManager
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiClientAhp
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.Ahp
import id.co.rumahcoding.sd.models.Recommendation
import kotlinx.android.synthetic.main.fragment_output_ahptraveller.*
import kotlinx.android.synthetic.main.fragment_output_ahptraveller.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.github.mikephil.charting.components.XAxis
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import id.co.rumahcoding.sd.models.RecommendationElement
import retrofit2.create


/**
 * A simple [Fragment] subclass.
 */
class OutputAHPTravellerFragment : Fragment() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: OutputAHPAdapter
    lateinit var barChart: BarChart

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_output_ahptraveller, container, false)

        val sessionManager = SessionManager(this.requireContext())
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val respondentId = activity?.intent!!.getStringExtra("respondentId")
        val token = sessionManager.fetchAuthToken()

        view.output_ahp_traveller.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this.requireContext())
        view.output_ahp_traveller.layoutManager = layoutManager

        apiEndPoint.recommendation2("Bearer $token", respondentId).enqueue(object: Callback<Recommendation>{
            override fun onFailure(call: Call<Recommendation>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<Recommendation>,
                response: Response<Recommendation>
            ) {
                val saveResponse = response.body()

                barChart = barChartAhp

                val valueList = ArrayList<Double>()
                val entries = ArrayList<BarEntry>()
                val labels = ArrayList<String>()
                val title = "AHP"

                //input data
                saveResponse!!.data?.forEach {
                    valueList.add(it.score.toDouble())
                    labels.add(it.attraction)
                }
                Log.d("LABELS", labels.toString())
                //fit the data into a bar
                for (i in valueList.indices) {
                    val barEntry = BarEntry(i.toFloat(), valueList[i].toFloat())
                    entries.add(barEntry)
                }

                val barDataSet = BarDataSet(entries, title)
                barDataSet.color = resources.getColor(R.color.colorAccent)

                val data = BarData(barDataSet)
                val xAxis = barChart.getXAxis()
                xAxis.setGranularity(1f)
                xAxis.setGranularityEnabled(true)
                xAxis.setCenterAxisLabels(true)
                xAxis.setDrawGridLines(true)
                xAxis.labelRotationAngle = 45f
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM)
                xAxis.setValueFormatter(IndexAxisValueFormatter(labels))

                barChart.data = data
                barChart.animateY(2000)
                barChart.invalidate()

                val adapter = OutputAHPAdapter(saveResponse.data as MutableList<RecommendationElement>)
                adapter.notifyDataSetChanged()
                output_ahp_traveller.adapter = adapter
            }
        })

        return view
    }
}
