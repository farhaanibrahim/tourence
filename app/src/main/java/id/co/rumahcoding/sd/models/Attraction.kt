package id.co.rumahcoding.sd.models

import com.google.gson.annotations.SerializedName

data class Attraction (

    @SerializedName("id") var id : Int,
    @SerializedName("user_id") var user_id : Int,
    @SerializedName("city_id") var city_id : Int,
    @SerializedName("attraction_name") var attraction_name : String,
    @SerializedName("price") var price : Int,
    @SerializedName("price_between") var price_between : Int,
    @SerializedName("distance_between") var distance_between: Int,
    @SerializedName("time_between") var time_between: Int
) {
    companion object {
        var city_id = 0
        var attraction_name = ""
        var price = 0
    }
}