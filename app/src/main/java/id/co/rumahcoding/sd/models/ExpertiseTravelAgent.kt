package id.co.rumahcoding.sd.models

data class ExpertiseTravelAgent (var success: Boolean, var message: String, var user: User)