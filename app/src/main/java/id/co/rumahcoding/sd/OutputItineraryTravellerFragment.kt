package id.co.rumahcoding.sd


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonParser
import id.co.rumahcoding.sd.adapters.OutputItineraryAdapter
import id.co.rumahcoding.sd.utils.SessionManager
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.Itenerary
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.models.AttractionList
import id.co.rumahcoding.sd.models.ItineraryOutput
import kotlinx.android.synthetic.main.fragment_output_itinerary_traveller.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class OutputItineraryTravellerFragment : Fragment() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: OutputItineraryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_output_itinerary_traveller, container, false)

        val sessionManager = SessionManager(this.requireContext())
        val token = sessionManager.fetchAuthToken()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val respondentId = activity?.intent?.getStringExtra("respondentId")
        view.output_itinerary_traveller.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this.requireContext())
        view.output_itinerary_traveller.layoutManager = layoutManager

        apiEndPoint.itineraryOutput("Bearer $token", respondentId).enqueue(object: Callback<ItineraryOutput>{
            override fun onFailure(call: Call<ItineraryOutput>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ItineraryOutput>, response: Response<ItineraryOutput>) {
                val saveResponse = response.body()
                view.max_value.text = saveResponse!!.data.max

                adapter = OutputItineraryAdapter(saveResponse?.data!!.attractions as MutableList<AttractionList>)
                adapter.notifyDataSetChanged()
                view.output_itinerary_traveller.adapter = adapter
            }

        })

        return view
    }


}
