package id.co.rumahcoding.sd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.Attraction
import kotlinx.android.synthetic.main.card_output_ahp_traveller.view.*

class OutputAttractionListAdapter(private val attractionList: List<Attraction>) : RecyclerView.Adapter<OutputAttractionListAdapter.AttractionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttractionViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_output_ahp_traveller, parent, false)

        return AttractionViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return attractionList.size
    }

    override fun onBindViewHolder(holder: AttractionViewHolder, position: Int) {
        val currentItem = attractionList[position]
        holder.attractionName.text = currentItem.attraction_name
    }

    class AttractionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val attractionName: TextView = itemView.attraction_name
    }
}