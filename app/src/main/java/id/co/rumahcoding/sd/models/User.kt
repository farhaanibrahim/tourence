package id.co.rumahcoding.sd.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(var id: Int, var name: String, var phone: String, var email: String, var roleId: Int, var expertise: Int) :
    Parcelable