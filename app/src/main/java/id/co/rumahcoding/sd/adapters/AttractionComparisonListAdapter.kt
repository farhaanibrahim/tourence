package id.co.rumahcoding.sd.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.AttractionComparison
import kotlinx.android.synthetic.main.card_attraction_comparison.view.*

class AttractionComparisonListAdapter(private val context: Context, private val attractionComparisonList: List<AttractionComparison>) : RecyclerView.Adapter<AttractionComparisonListAdapter.AttractionComparisonViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AttractionComparisonViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_attraction_comparison, parent, false)
        return AttractionComparisonViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return attractionComparisonList.size
    }

    override fun onBindViewHolder(holder: AttractionComparisonViewHolder, position: Int) {
        val currentItem = attractionComparisonList[position]
        holder.initial_attraction.text = currentItem.initial_attraction
        holder.final_attraction.text = currentItem.final_attraction
        holder.price.text = currentItem.price.toString()
        holder.distance.text = currentItem.distance.toString()
        holder.time.text = currentItem.distance.toString()
    }

    class AttractionComparisonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val initial_attraction: TextView = itemView.initial_attraction
        val final_attraction: TextView = itemView.final_attraction
        val price: TextView = itemView.price
        val distance: TextView = itemView.distance
        val time: TextView = itemView.time
    }

}