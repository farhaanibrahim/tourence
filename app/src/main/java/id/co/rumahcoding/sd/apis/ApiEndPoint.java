package id.co.rumahcoding.sd.apis;

import android.service.autofill.UserData;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import id.co.rumahcoding.sd.apis.responses.AddAttractionResponse;
import id.co.rumahcoding.sd.apis.responses.LoginResponse;
import id.co.rumahcoding.sd.apis.responses.RespondentResponse;
import id.co.rumahcoding.sd.apis.responses.SaveRespondentResponse;
import id.co.rumahcoding.sd.apis.responses.SaveResponse;
import id.co.rumahcoding.sd.apis.responses.UserDataResponse;
import id.co.rumahcoding.sd.models.Ahp;
import id.co.rumahcoding.sd.models.Attraction;
import id.co.rumahcoding.sd.models.AttractionComparison;
import id.co.rumahcoding.sd.models.BestRoute;
import id.co.rumahcoding.sd.models.City;
import id.co.rumahcoding.sd.models.Expertise;
import id.co.rumahcoding.sd.models.ExpertiseTravelAgent;
import id.co.rumahcoding.sd.models.Itenerary;
import id.co.rumahcoding.sd.models.ItineraryOutput;
import id.co.rumahcoding.sd.models.MyCity;
import id.co.rumahcoding.sd.models.MyCityExpertise;
import id.co.rumahcoding.sd.models.Recommendation;
import id.co.rumahcoding.sd.models.ResponseHistory;
import id.co.rumahcoding.sd.models.Role;
import id.co.rumahcoding.sd.models.TravelAgent;
import id.co.rumahcoding.sd.models.User;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * Created by blastocode on 9/13/17.
 */

public interface ApiEndPoint {
    @FormUrlEncoded
    @POST("respondents/submit")
    Call<SaveRespondentResponse> save(
                            @Header("Authorization") String token,
                            @Field("respondent_type") String respondentType,
                            @Field("user_id") String user_id,
                            @Field("uid_travel_agent") Integer uid_travel_agent,
                            @Field("number_of_person") Integer numberOfPerson,
                            @Field("traveling_budget") Integer travelingBudget,
                            @Field("day") Integer day,
                            @Field("night") Integer night,
                            @Field("traveling_start_date") String travelingStartDate,
                            @Field("traveling_end_date") String travelingEndDate,
                            @Field("city_id") String city_id,
                            @Field("gender_and_age") String gender_and_age,
                            @Field("decisions") String decisions);

    @FormUrlEncoded
    @POST("respondent/detail")
    Call<RespondentResponse> respondentDetail(
            @Header("Authorization") String token,
            @Field("respondent_id") Integer respondent_id
    );

    @FormUrlEncoded
    @POST("expertise/check")
    Call<SaveResponse> check(
            @Header("Authorization") String token,
            @Field("uIdTravelAgent") Integer xid);

    @FormUrlEncoded
    @POST("expertise/assign")
    Call<SaveResponse> assign(
            @Header("Authorization") String token,
            @Field("email") String email,
            @Field("user_travel_agent") int user_travel_agent,
            @Field("selected_city") String selected_city
    );

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @POST("account")
    Call<UserDataResponse> userData(@Header("Authorization") String token);

    @POST("fetch-all-user")
    Call<List<User>> getAllUser(@Header("Authorization") String token);

    @GET("attractions/{city_id}/{user_id}")
    Call<List<Attraction>> attractionData(
            @Path("city_id") Integer city_id,
            @Path("user_id") Integer user_id
    );

    @GET("attractions2/{city_id}/{user_id}")
    Call<List<Attraction>> attractionData2(
            @Header("Authorization") String token,
            @Path("city_id") Integer city_id,
            @Path("user_id") Integer user_id
    );

    @GET("attractions-check/{city_id}/{user_id}")
    Call<SaveResponse> attractionCheck(
            @Header("Authorization") String token,
            @Path("city_id") Integer city_id,
            @Path("user_id") Integer user_id
    );

    @FormUrlEncoded
    @POST("attractions/add")
    Call<AddAttractionResponse> addAttraction(
            @Header("Authorization") String token,
            @Field("user_id") Integer user_id,
            @Field("attraction_name") String attraction_name,
            @Field("city_id") Integer city_id,
            @Field("attraction_price") Integer attraction_price,
            @Field("attraction") String attraction
    );

    @FormUrlEncoded
    @POST("attraction/detail")
    Call<List<AttractionComparison>> attractionComparison(
            @Header("Authorization") String token,
            @Field("user_id") Integer user_id,
            @Field("attraction_id") Integer attraction_id
    );

    @GET("top-travel-agent")
    Call<List<TravelAgent>> travelAgentData();

    @GET("city")
    Call<List<City>> fetchCity();

    @GET("my-city/{user_id}")
    Call<List<MyCity>> mycity(
            @Path("user_id") Integer user_id
    );

    @FormUrlEncoded
    @POST("my-city/add")
    Call<SaveResponse> addCity(
            @Header("Authorization") String token,
            @Field("user") String user,
            @Field("city") String city
    );

    @POST("travel-agent")
    Call<List<TravelAgent>> travelAgentData(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("expertise")
    Call<List<Expertise>> expertiseData(
            @Header("Authorization") String token,
            @Field("user_travel_agent") Integer user_travel_agent
    );

    @FormUrlEncoded
    @POST("expertise/detail")
    Call<Expertise> expertiseDetail(
            @Header("Authorization") String token,
            @Field("user_expertise") Integer user_expertise
    );

    @GET("roles")
    Call<List<Role>> roleData();

    @FormUrlEncoded
    @POST("registration")
    Call<SaveResponse> registration(
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("expertise-travel-agent")
    Call<ExpertiseTravelAgent> getTravelAgent(
            @Header("Authorization") String token,
            @Field("user_expertise") Integer user_expertise
    );

    @FormUrlEncoded
    @POST("my-city/expertise")
    Call<List<MyCityExpertise>> myCityExpertise(
            @Header("Authorization") String token,
            @Field("user_expertise_id") Integer user_expertise_id
    );

    @GET("generate")
    Call<BestRoute> bestRoute();

    @FormUrlEncoded
    @POST("respondent-history")
    Call<ResponseHistory> myResponseHistory(
            @Header("Authorization") String token,
            @Field("user_id") Integer user_id
    );

    @FormUrlEncoded
    @POST("respondent-matrix")
    Call<JsonObject> respondentMatrix(
            @Header("Authorization") String token,
            @Field("respondent_id") Integer respondentId
    );

    @POST("recommendation")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<Ahp> recommendation(
            @Body JsonObject matrix
    );

    @FormUrlEncoded
    @POST("save-recommendation")
    Call<SaveResponse> saveRecommendation(
            @Header("Authorization") String token,
            @Field("recommendation") String recommendation,
            @Field("respondent_id") String respondentId,
            @Field("uid_travel_agent") String uidTravelAgent,
            @Field("city_id") String cityId
    );

    @FormUrlEncoded
    @POST("output-ahp")
    Call<Recommendation> recommendation2(
            @Header("Authorization") String token,
            @Field("respondent_id") String respondentId
    );

    @FormUrlEncoded
    @POST("save-itinerary")
    Call<SaveResponse> saveItinerary(
            @Header("Authorization") String token,
            @Field("respondent_id") String respondentId,
            @Field("itineraries") String itineraries,
            @Field("max") String max
    );

    @FormUrlEncoded
    @POST("itinerary-output")
    Call<ItineraryOutput> itineraryOutput(
            @Header("Authorization") String token,
            @Field("respondent_id") String respondent_id
    );

    @POST("itenerary")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<Itenerary> itenerary(
            @Body JsonObject city_id
    );

    @FormUrlEncoded
    @POST("check-expertise-response")
    Call<SaveResponse> checkExpertiseResponse(
            @Header("Authorization") String token,
            @Field("uid_travel_agent") String uid_travel_agent,
            @Field("city_id") String city_id
    );
}