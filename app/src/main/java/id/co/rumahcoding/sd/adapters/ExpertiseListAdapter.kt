package id.co.rumahcoding.sd.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.ExpertiseDetail
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.CityExpertise
import id.co.rumahcoding.sd.models.Expertise
import id.co.rumahcoding.sd.models.MyCity
import kotlinx.android.synthetic.main.card_expertise_layout.view.*
import kotlinx.android.synthetic.main.card_travel_agent_layout.view.*
import kotlinx.android.synthetic.main.card_travel_agent_layout.view.user_email
import kotlinx.android.synthetic.main.card_travel_agent_layout.view.user_name
import kotlinx.android.synthetic.main.card_travel_agent_layout.view.user_phone

class ExpertiseListAdapter(private val context: Context, private val expertiseList: List<Expertise>) : RecyclerView.Adapter<ExpertiseListAdapter.ExpertiseViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpertiseViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_expertise_layout, parent, false)

        return ExpertiseViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return expertiseList.size
    }

    override fun onBindViewHolder(holder: ExpertiseViewHolder, position: Int) {
        val currentItem = expertiseList[position]

        holder.userName.text = currentItem.expertise.name
        holder.userPhone.text = currentItem.expertise.phone
        holder.userEmail.text = currentItem.expertise.email
        holder.cardExpertise.setOnClickListener {
            val intent = Intent(context, ExpertiseDetail::class.java)
            intent.putExtra("user_expertise_id", currentItem.user_expertise.id)
            context.startActivity(intent)
        }
    }

    class ExpertiseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val userName: TextView = itemView.user_name
        val userPhone: TextView = itemView.user_phone
        val userEmail: TextView = itemView.user_email
        val cardExpertise: CardView = itemView.card_expertise
    }
}