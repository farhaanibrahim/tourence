package id.co.rumahcoding.sd.utils

import android.content.Context
import android.content.SharedPreferences
import id.co.rumahcoding.sd.R

class SessionManager(context: Context) {
    private var prefs: SharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)

    companion object{
        const val USER_TOKEN = "user_token"
        const val USER_ID = "user_id"
        const val USER_NAME = "Guest"
        const val USER_PHONE = "user_phone"
        const val USER_EMAIL = "user_email"
        const val USER_ROLE_ID = "user_role"
        const val USER_EXPERTISE = "user_expertise"
    }

    /**
     * Function to save auth token
     */
    fun saveAuthToken(token: String, id: Int, name: String, phone: String, email: String, role: String, expertise: String){
        val editor = prefs.edit()
        editor.putString(USER_TOKEN, token)
            .putInt(USER_ID, id)
            .putString(USER_NAME, name)
            .putString(USER_PHONE, phone)
            .putString(USER_EMAIL, email)
            .putString(USER_ROLE_ID, role)
            .putString(USER_EXPERTISE, expertise)
            .apply()
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String? {
        return prefs.getString(USER_TOKEN, null)
    }

    fun fetchId(): Int {
        return prefs.getInt(USER_ID, 0)
    }

    fun fetchName(): String? {
        return prefs.getString(USER_NAME, null)
    }

    fun fetchPhone(): String? {
        return prefs.getString(USER_PHONE, null)
    }

    fun fetchEmail(): String? {
        return prefs.getString(USER_EMAIL, null)
    }

    fun fetchRole(): String? {
        return prefs.getString(USER_ROLE_ID, null)
    }

    fun fetchExpertise(): String? {
        return prefs.getString(USER_EXPERTISE, null)
    }

    fun removeToken(){
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
    }
}