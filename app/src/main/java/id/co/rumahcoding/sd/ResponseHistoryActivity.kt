package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.rumahcoding.sd.adapters.ResponseHistoryListAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.Respondent
import id.co.rumahcoding.sd.models.ResponseHistory
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_response_history.*
import kotlinx.android.synthetic.main.card_response_history.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResponseHistoryActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: ResponseHistoryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_response_history)

        app_bar.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        val sessionManager = SessionManager(this)
        val userId = sessionManager.fetchId()
        val token = sessionManager.fetchAuthToken()
//        val expertise = sessionManager.fetchExpertise()

//        if (expertise == "1"){
//            budget_info.visibility = View.INVISIBLE
//            number_of_person_info.visibility = View.INVISIBLE
//            duration_info.visibility = View.INVISIBLE
//        }

        response_history_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        response_history_list.layoutManager = layoutManager

        fetchResponseHistory(userId, token.toString())
    }

    private fun fetchResponseHistory(userId: Int, token: String) {
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.myResponseHistory("Bearer $token", userId)
        call.enqueue(object: Callback<ResponseHistory>{
            override fun onFailure(call: Call<ResponseHistory>, t: Throwable) {
                Toast.makeText(applicationContext,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<ResponseHistory>,
                response: Response<ResponseHistory>
            ) {
                adapter = ResponseHistoryListAdapter(this@ResponseHistoryActivity, response.body()?.respondent as MutableList<Respondent>)
                adapter.notifyDataSetChanged()
                response_history_list.adapter = adapter
            }

        })
    }
}
