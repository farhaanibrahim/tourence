package id.co.rumahcoding.sd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.models.AttractionList
import id.co.rumahcoding.sd.models.Itenerary
import kotlinx.android.synthetic.main.card_output_itinerary_traveller.view.*
import kotlinx.android.synthetic.main.fragment_output_itinerary_traveller.view.*

class OutputItineraryAdapter(private val attractionList: List<AttractionList>): RecyclerView.Adapter<OutputItineraryAdapter.ItineraryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItineraryViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_output_itinerary_traveller, parent, false)

        return ItineraryViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return attractionList.size
    }

    override fun onBindViewHolder(holder: ItineraryViewHolder, position: Int) {
        val currentItem = attractionList[position]
        holder.attractionName.text = currentItem.attraction
    }

    class ItineraryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val attractionName: TextView = itemView.attraction_name
    }

}