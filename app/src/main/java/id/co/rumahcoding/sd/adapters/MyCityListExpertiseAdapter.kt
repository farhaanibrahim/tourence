package id.co.rumahcoding.sd.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.LoginActivity
import id.co.rumahcoding.sd.MainActivity
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.UserDataResponse
import id.co.rumahcoding.sd.models.Attraction
import id.co.rumahcoding.sd.models.GeneralInfo
import id.co.rumahcoding.sd.models.MyCityExpertise
import id.co.rumahcoding.sd.utils.PopupUtil
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_select_city.view.*
import kotlinx.android.synthetic.main.card_city_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



class MyCityListExpertiseAdapter(private val context: Context, private val uIdTravelAgent: Int, private val myCityList: List<MyCityExpertise>) : RecyclerView.Adapter<MyCityListExpertiseAdapter.MyCityExpertiseViewHolder>() {

    private var attractionList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyCityExpertiseViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_city_layout, parent, false)

        return MyCityExpertiseViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return myCityList.size
    }

    override fun onBindViewHolder(holder: MyCityExpertiseViewHolder, position: Int) {
        val currentItem = myCityList[position]

        holder.cityName.text = currentItem.city.city_name
        holder.itemView.setOnClickListener {
            GeneralInfo.city = currentItem.city.city_name
            GeneralInfo.cityid = currentItem.city.id

            val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
            Log.d("UID TRAVEL AGENT", uIdTravelAgent.toString())
            val call = apiEndPoint.attractionData(GeneralInfo.cityid, uIdTravelAgent)
            call.enqueue(object: Callback<MutableList<Attraction>>{
                override fun onFailure(call: Call<MutableList<Attraction>>, t: Throwable) {
                    Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<MutableList<Attraction>>,
                    response: Response<MutableList<Attraction>>
                ) {
                    if (response.body()!!.size == 0) {
                        Toast.makeText(context, "Your travel agent has no attraction in ${currentItem.city.city_name}", Toast.LENGTH_LONG).show()
                    } else {
                        for (i in 0 until response.body()!!.size) {
                            attractionList.add(response.body()?.get(i)!!.attraction_name)
                        }
                        GeneralInfo.attractions = attractionList

                        loginSessionCheck()
                    }
                }

            })
        }
    }

    class MyCityExpertiseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cityName: TextView = itemView.city_name
    }

    private fun loginSessionCheck() {
        val sessionManager = SessionManager(context)
        val userToken = sessionManager.fetchAuthToken()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.userData("Bearer ${userToken}")
        call.enqueue(object: Callback<UserDataResponse>{
            override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                Snackbar.make(View(context).root_layout_select_city, t.message.toString(), Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<UserDataResponse>,
                response: Response<UserDataResponse>
            ) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if (success == true) {

                    expertiseValidation(uIdTravelAgent)

                } else {
                    Toast.makeText(context, "You are not registered, please login", Toast.LENGTH_LONG).show()
                }
            }

        })
    }

    fun expertiseValidation(uIdTravelAgent: Int) {
        val sessionManager = SessionManager(context)
        val expertise = sessionManager.fetchExpertise()

        if (expertise == "1"){
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra("uIdTravelAgent", uIdTravelAgent)
            intent.putExtra("expertise", true)

            context.startActivity(intent)
        } else {
            PopupUtil.showMsg(context, "You are not assigned as expertise user", PopupUtil.SHORT)
        }
    }
}