package id.co.rumahcoding.sd.models

data class Recommendation (
    val success: Boolean,
    val message: String,
    val data: List<RecommendationElement>? = null
)

data class RecommendationElement (
    val id: Int,
    val respondentID: Int,
    val attraction: String,
    val score: String,
    val createdAt: String,
    val updatedAt: String
)