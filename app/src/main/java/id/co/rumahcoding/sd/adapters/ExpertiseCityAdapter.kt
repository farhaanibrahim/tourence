package id.co.rumahcoding.sd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.CityExpertise
import kotlinx.android.synthetic.main.item_city.view.*

class ExpertiseCityAdapter(private val cityList: MutableList<CityExpertise>): RecyclerView.Adapter<ExpertiseCityAdapter.ExpertiseCityViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpertiseCityViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_city, parent, false)
        return ExpertiseCityViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    override fun onBindViewHolder(holder: ExpertiseCityViewHolder, position: Int) {
        val currentItem = cityList[position]
        holder.nameOfCity.text = currentItem.city.city_name
    }

    class ExpertiseCityViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val nameOfCity: TextView = itemView.name_of_city
    }

}