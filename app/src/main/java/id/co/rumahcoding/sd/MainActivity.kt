package id.co.rumahcoding.sd

import android.content.DialogInterface
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import com.beust.klaxon.Klaxon
import id.co.rumahcoding.sd.models.Decision
import id.co.rumahcoding.sd.utils.PopupUtil
import kotlinx.android.synthetic.main.activity_main.*
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.ApiClient
import retrofit2.Callback
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import retrofit2.Call
import retrofit2.Response
import id.co.rumahcoding.sd.models.GeneralInfo
import id.co.rumahcoding.sd.utils.Connectivity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import id.co.rumahcoding.sd.apis.ApiClientAhp
import id.co.rumahcoding.sd.apis.responses.SaveRespondentResponse
import id.co.rumahcoding.sd.models.Ahp
import id.co.rumahcoding.sd.models.Itenerary
import id.co.rumahcoding.sd.utils.SessionManager


class MainActivity : AppCompatActivity(), DecisionFragment.OnFragmentInteractionListener {
    private var travelerDecisions:List<List<String>> = listOf(
        listOf("Priority Attraction"),
        listOf("Satisfaction", "Experience", "Insight"),
        listOf("Preferences", "Popularity", "Accessibility", "Accomodation", "Facility", "Information", "Distance", "Time", "Cost"),
        listOf("Geophysical-Landscape\nAesthetic", "Ecological-Biological", "Cultural-Historical", "Recreational"))

    /*private var expertiseDecisions:List<List<String>> = listOf(
        listOf("Geophysical-Landscape\nAesthetic", "Ecological-Biological", "Cultural-Historical", "Recreational"),
        listOf("Pandawa Beach", "Kuta Beach", "Sanur Beach", "Ulu Watu Beach", "Lovina Beach", "Tegal Wangi Beach", "Seminyak Beach", "Canggu Beach", "Amed Beach", "Crystal Bay Beach", "Benoa Water\nSports", "Jimbaran Beach", "Balangan Beach", "Dreamland Beach", "Padang Padang\nBeach", "Legian Beach", "Double Six\nBeach", "Tuban Beach", "White Sand\nBeach", "Nusa Penida\nIsland", "Dream Beach,\nNusa Lembongan", "Melasti Beach", "Menjangan Island", "Secret Garden\nVillage", "Turtle Island", "Garuda Wisnu\nKencana Cultural\nPark", "Tirta Gangga", "Besakih Temple", "Uluwatu Temple", "Tirta Empul\nTemple", "Lempuyang Temple", "Tanah Lot\nTemple", "Taman Ayun\nTemple", "Beji Guwang\nHidden Canyon", "Ulun Danu\nBratan Temple", "Luhur Batukaru\nTemple", "Yeh Mempeh\nWaterfall", "Sangeh Monkey\nForest", "Lake Batur\nKintamani", "Lake Beratan\nBedugul", "Telaga Waja\nRiver", "Ayung Bali\nRafting", "Waterbom Bali", "Bali Bird\nPark", "Bali Safari &\nMarine Park", "West Bali\nNational Park\nForest", "Elephant Safari\nPark", "Bali Botanic\nGarden", "Tukad Cepung\nWaterfall", "Bali Zoo"))
    */

    private var expertiseDecisions = mutableListOf(
        listOf("Geophysical-Landscape\nAesthetic", "Ecological-Biological", "Cultural-Historical", "Recreational"))

    private var decisions: List<List<String>> = emptyList()

    private var decisionIndex = 0
    private var decisionSubIndex = 0
    private var scoredDecisions = mutableListOf<Decision>()
    private var fragment: Fragment? = null

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        app_bar_main.setOnClickListener {
            val builder = AlertDialog.Builder(this@MainActivity)
                .setTitle("Are you sure?")
                .setMessage("If you back, your work will be lost, are you sure?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener{
                        _, _ -> startActivity(Intent(this@MainActivity, HomeActivity::class.java))
                })
                .setNegativeButton("No", DialogInterface.OnClickListener{
                        dialog, _ -> dialog.dismiss()
                })
                .create()
            builder.show()
        }

        if(savedInstanceState != null) {
            decisionIndex = savedInstanceState.getInt("decisionIndex")
            decisionSubIndex = savedInstanceState.getInt("decisionSubIndex")
        }

        val sessionManager = SessionManager(this)
        val role = sessionManager.fetchRole()
        val expertiseValidation = intent.getBooleanExtra("expertise", false)

        if(expertiseValidation != true) {
            decisions = travelerDecisions
        } else {
            expertiseDecisions.add(GeneralInfo.attractions)
            decisions = expertiseDecisions
        }

        fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)

        if(fragment == null) {
            val decision = getDecision(decisionIndex, decisionSubIndex)
            fragment = DecisionFragment.newInstance(decision)

            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment as DecisionFragment)
                //.addToBackStack("decision")
                .commit()
        }

        prevButton.setOnClickListener {
            prevFragment()
        }

        nextButton.setOnClickListener {
            nextFragment()
        }

        finishButton.setOnClickListener {
            //Log.d("MainActivity", Klaxon().toJsonString(scoredDecisions))
            if(fragment != null) {
                scoredDecisions.add((fragment as DecisionFragment).decision!!)
            }

            if(Connectivity.isConnected(applicationContext)) {
                saveData()
            }
            else {
                PopupUtil.showMsg(applicationContext, "Please check your internet connection", PopupUtil.SHORT)
            }
        }

        refreshButtons()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            val builder = AlertDialog.Builder(this@MainActivity)
                .setTitle("Are you sure?")
                .setMessage("If you back, your work will be lost, are you sure?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener{
                    _, _ -> startActivity(Intent(this@MainActivity, HomeActivity::class.java))
                })
                .setNegativeButton("No", DialogInterface.OnClickListener{
                        dialog, _ -> dialog.dismiss()
                })
                .create()
            builder.show()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putInt("decisionIndex", decisionIndex)
        outState.putInt("decisionSubIndex", decisionSubIndex)
    }

    private fun getDecision(index: Int, subIndex: Int): Decision? {
        if(index < 0 || index >= decisions.lastIndex) {
            return null
        }

        if(subIndex < 0 || subIndex > decisions[index].lastIndex) {
            return null
        }

        val root = decisions[index][subIndex]

        //if(index < travelerDecisions.lastIndex){
        val children = decisions[index + 1]
        //}

        val scalarIndex = toScalarIndex(decisionIndex, decisionSubIndex)

        if(scoredDecisions.lastIndex >= scalarIndex) {
            return scoredDecisions[scalarIndex]
        }

        //Log.d("MainActivity", "index: $decisionIndex, subIndex: $decisionSubIndex")
        //Log.d("MainActivity", "root: $root, children: $children")

        return Decision(root, children)
    }

    private fun prevFragment() {
        if(fragment != null) {
            if(scoredDecisions.lastIndex < toScalarIndex(decisionIndex, decisionSubIndex)) {
                scoredDecisions.add((fragment as DecisionFragment).decision!!)
            }
        }

        var decision:Decision? = null

        if(decisionSubIndex > 0) {
            decisionSubIndex--
            decision = getDecision(decisionIndex, decisionSubIndex)
        }
        else if(decisionSubIndex == 0) {
            if(decisionIndex > 0) {
                decisionIndex--
                decisionSubIndex = 0
                decision = getDecision(decisionIndex, decisionSubIndex)
            }
        }

        if(decision != null) {
            fragment = DecisionFragment.newInstance(decision)

            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment as DecisionFragment)
                //.addToBackStack("decision")
                .commit()
        }
        else {
            //Toast.makeText(this@MainActivity, "Invalid decision", Toast.LENGTH_SHORT).show()
        }

        refreshButtons()
    }

    private fun nextFragment() {
        if(fragment != null) {
            if(scoredDecisions.lastIndex < toScalarIndex(decisionIndex, decisionSubIndex)) {
                scoredDecisions.add((fragment as DecisionFragment).decision!!)
            }
        }

        var decision:Decision? = null

        if(decisionSubIndex < decisions[decisionIndex].lastIndex) {
            decisionSubIndex++
            decision = getDecision(decisionIndex, decisionSubIndex)
        }
        else if(decisionSubIndex == decisions[decisionIndex].lastIndex) {
            if(decisionIndex < decisions.lastIndex - 1) {
                decisionIndex++
                decisionSubIndex = 0
                decision = getDecision(decisionIndex, decisionSubIndex)
            }
        }

        if(decision != null) {
            fragment = DecisionFragment.newInstance(decision)

            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment as DecisionFragment)
                //.addToBackStack("decision")
                .commit()
        }
        else {
            //Toast.makeText(this@MainActivity, "Invalid decision", Toast.LENGTH_SHORT).show()
        }

        refreshButtons()
    }

    override fun onBackPressed() {
        val uIdTravelAgent = intent.getIntExtra("uIdTravelAgent", 0)
        val intent = Intent(this@MainActivity, HomeActivity::class.java)
        intent.putExtra("uIdTravelAgent", uIdTravelAgent)
        startActivity(intent)
    }

    private fun refreshButtons() {
        prevButton.visibility = View.GONE
        nextButton.visibility = View.GONE
        finishButton.visibility = View.GONE

        if(decisionIndex > 0 || decisionSubIndex > 0) {
            prevButton.visibility = View.VISIBLE
        }

        if((decisionIndex == decisions.lastIndex - 1) && (decisionSubIndex == decisions[decisions.lastIndex - 1].lastIndex)) {
            finishButton.visibility = View.VISIBLE
        }
        else {
            nextButton.visibility = View.VISIBLE
        }
    }

    private fun toScalarIndex(decisonIndex: Int, decisonSubIndex:Int): Int {
        var index = -1

        decisions.forEachIndexed { index1, list1 ->
            list1.forEachIndexed { index2, list2 ->
                index++

                if(decisonIndex == index1 && decisionSubIndex == index2) {
                    return index
                }
            }
        }

        return index
    }

    private fun saveData() {
        finishButton.isEnabled = false;
        PopupUtil.showLoading(this, "Please wait", "Saving ...")

        val decisionJson = Klaxon().toJsonString(scoredDecisions)
        val genderAndAge = Klaxon().toJsonString(GeneralInfo.generalInfos)

        val expertiseValidation = intent.getBooleanExtra("expertise", false)

        Log.d("MainActivity", "decisions: $decisionJson")
        Log.d("MainActivity", "ageAndGender: $genderAndAge")

        val sessionManager = SessionManager(this)
        val userId = sessionManager.fetchId().toString()
        val respondentType = sessionManager.fetchExpertise()
        Log.d("respondentType : ", respondentType)
        val uIdTravelAgent = intent.getIntExtra("uIdTravelAgent", 0)
        Log.d("MainActivity", uIdTravelAgent.toString())
        val token = sessionManager.fetchAuthToken()
        val cityId = GeneralInfo.cityid.toString()
        Log.d("MainActivity", "cityId: $cityId")
        val travelingStartDate = GeneralInfo.travelingStartDate
        val travelingEndDate = GeneralInfo.travelingEndDate

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.save("Bearer ${token}",respondentType, userId, uIdTravelAgent, GeneralInfo.numberOfPerson, GeneralInfo.travelingBudget, GeneralInfo.day,GeneralInfo.night, travelingStartDate, travelingEndDate, cityId, genderAndAge, decisionJson)

        call.enqueue(object : Callback<SaveRespondentResponse> {
            override fun onFailure(call: Call<SaveRespondentResponse>, t: Throwable) {
                finishButton.isEnabled = true
                Log.d("ERROR", t.message)
                PopupUtil.showMsg(this@MainActivity, t.message, PopupUtil.SHORT)
                PopupUtil.dismissDialog()
            }

            override fun onResponse(call: Call<SaveRespondentResponse>, response: Response<SaveRespondentResponse>) {
                val saveResponse = response.body()
                val message = saveResponse?.message
                val respondentId = saveResponse?.respondentId

                Log.d("status ", saveResponse?.success.toString())

                if(saveResponse != null) {
                    if (saveResponse.success) {
                        PopupUtil.showMsg(this@MainActivity, "Data successfully saved", PopupUtil.SHORT)

                        GeneralInfo.numberOfPerson = 0
                        GeneralInfo.travelingBudget = 0
                        GeneralInfo.day = 0
                        GeneralInfo.night = 0
                        GeneralInfo.travelingStartDate = ""
                        GeneralInfo.travelingEndDate = ""
                        GeneralInfo.generalInfos = mutableListOf()
                        GeneralInfo.city = ""
                        GeneralInfo.cityid = 0
                        GeneralInfo.attractions = mutableListOf()

                        if (expertiseValidation == true) {
                            val intent = Intent(this@MainActivity, OutputExpertiseActivity::class.java)
                            intent.putExtra("userId", userId)
                            intent.putExtra("respondentId", respondentId.toString())
                            intent.putExtra("cityId", cityId)
                            intent.putExtra("uidTravelAgent", uIdTravelAgent.toString())
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                            finish()
                        } else {
                            // Get Matrix
                            apiEndPoint.respondentMatrix("Bearer $token", respondentId).enqueue(object: Callback<JsonObject>{
                                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                                    Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
                                }

                                override fun onResponse(
                                    call: Call<JsonObject>,
                                    response: Response<JsonObject>
                                ) {
                                    // Get recommendation
                                    val responseMatrix = response.body()
                                    ApiClientAhp.getClient().create(ApiEndPoint::class.java).recommendation(responseMatrix).enqueue(object: Callback<Ahp>{
                                        override fun onFailure(call: Call<Ahp>, t: Throwable) {
                                            Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
                                        }

                                        override fun onResponse(
                                            call: Call<Ahp>,
                                            response: Response<Ahp>
                                        ) {
                                            // Save recommendation
                                            val recommendation = Klaxon().toJsonString(response.body()?.recommendation)
                                            apiEndPoint.saveRecommendation("Bearer $token", recommendation, respondentId.toString(), uIdTravelAgent.toString(), cityId)
                                                .enqueue(object: Callback<SaveResponse>{
                                                    override fun onFailure(
                                                        call: Call<SaveResponse>,
                                                        t: Throwable
                                                    ) {
                                                        Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
                                                    }

                                                    override fun onResponse(
                                                        call: Call<SaveResponse>,
                                                        response: Response<SaveResponse>
                                                    ) {
                                                        // Get itinerary
                                                        val cityIdJsonString = "{\"city_id\": $cityId}"
                                                        val cityIdJsonObject = JsonParser().parse(cityIdJsonString)
                                                        ApiClientAhp.getClient().create(ApiEndPoint::class.java).itenerary(cityIdJsonObject.asJsonObject)
                                                            .enqueue(object: Callback<Itenerary>{
                                                                override fun onFailure(
                                                                    call: Call<Itenerary>,
                                                                    t: Throwable
                                                                ) {
                                                                    Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
                                                                }

                                                                override fun onResponse(
                                                                    call: Call<Itenerary>,
                                                                    response: Response<Itenerary>
                                                                ) {
                                                                    // Save Itinerary
                                                                    val itineraries = Klaxon().toJsonString(response.body()?.itenarary)
                                                                    val max = response.body()?.max
                                                                    Log.d("Itineraries", itineraries)
                                                                    Log.d("Max", max.toString())
                                                                    apiEndPoint.saveItinerary("Bearer $token", respondentId.toString(), itineraries, max.toString()).enqueue(object: Callback<SaveResponse>{
                                                                        override fun onFailure(
                                                                            call: Call<SaveResponse>,
                                                                            t: Throwable
                                                                        ) {
                                                                            Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
                                                                        }

                                                                        override fun onResponse(
                                                                            call: Call<SaveResponse>,
                                                                            response: Response<SaveResponse>
                                                                        ) {
                                                                            val intent = Intent(this@MainActivity, TravellerOutputActivity::class.java)
                                                                            intent.putExtra("respondentId", respondentId.toString())
                                                                            startActivity(intent)
                                                                        }

                                                                    })
                                                                }

                                                            })

                                                    }

                                                })
                                        }

                                    })
                                }

                            })
                        }
                    }
                    else {
                        PopupUtil.showMsg(this@MainActivity, message.toString(), PopupUtil.SHORT)
                    }
                }

                PopupUtil.dismissDialog()
            }
        })
    }
}