package id.co.rumahcoding.sd.models

import android.os.Parcelable
import androidx.versionedparcelable.ParcelField
import com.beust.klaxon.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
class Decision(val root: String, @Json(ignored = true) val children: List<String>, var comparisons: List<Comparison> = emptyList()): Parcelable