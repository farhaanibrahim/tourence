package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.beust.klaxon.Klaxon
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import id.co.rumahcoding.sd.utils.PopupUtil
import id.co.rumahcoding.sd.utils.SessionManager
import id.co.rumahcoding.sd.adapters.ChecklistCityAdapter
import id.co.rumahcoding.sd.models.MyCity
import kotlinx.android.synthetic.main.activity_assign_expertise.*
import kotlinx.android.synthetic.main.activity_assign_expertise.app_bar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AssignExpertiseActivity : AppCompatActivity() {
    var cityList: MutableList<MyCity> = mutableListOf()
    var selectedCity: MutableList<MyCity> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assign_expertise)

        val sessionManager = SessionManager(this)
        val userId = sessionManager.fetchId()

        check_list_city.setItemViewCacheSize(20)
        check_list_city.setHasFixedSize(true)

//        val cityId = ArrayList<String>()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)

        apiEndPoint.mycity(userId).enqueue(object: Callback<MutableList<MyCity>>{
            override fun onFailure(call: Call<MutableList<MyCity>>, t: Throwable) {
                Toast.makeText(this@AssignExpertiseActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<MutableList<MyCity>>, response: Response<MutableList<MyCity>>) {
                val saveResponse = response.body()
                for (i in 0 until saveResponse!!.size){
                    cityList.add(MyCity(saveResponse[i].id, saveResponse[i].city_name))
                }
                check_list_city.apply {
                    layoutManager = LinearLayoutManager(this@AssignExpertiseActivity)
                    adapter = ChecklistCityAdapter(cityList, selectedCity)
                }
            }

        })

        app_bar.setOnClickListener {
            onBackPressed()
        }

        assign_button.setOnClickListener {
            val citySelected = Klaxon().toJsonString(selectedCity)
            val email = emailExpertise.text.toString()
            Log.d("City Selected : ", citySelected)
//            val city_selected = city_spinner.selectedItemPosition
//            val cityIdSelected = cityId[city_selected]

            assignExpertise(email, citySelected)
        }
    }

    private fun assignExpertise(email: String, selectedCity: String){
        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()

        val travelAgentId = sessionManager.fetchId()

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.assign("Bearer $token", email, travelAgentId, selectedCity)
        call.enqueue(object: Callback<SaveResponse>{
            override fun onFailure(call: Call<SaveResponse>, t: Throwable) {
                PopupUtil.showMsg(this@AssignExpertiseActivity, "Your session login is expired", 3)
                startActivity(Intent(this@AssignExpertiseActivity, StartActivity::class.java))
                finish()
            }

            override fun onResponse(
                call: Call<SaveResponse>,
                response: Response<SaveResponse>
            ) {
                val saveResponse = response.body()
                val success = saveResponse?.success
                val message = saveResponse?.message

                if (success == true) {
                    PopupUtil.showMsg(this@AssignExpertiseActivity, message, 3)
                    val intent = Intent(this@AssignExpertiseActivity, ExpertiseActivity::class.java)
                    startActivity(intent)
                } else {
                    PopupUtil.showMsg(this@AssignExpertiseActivity, message, 3)
                }
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
