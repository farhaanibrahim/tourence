package id.co.rumahcoding.sd.models

import android.os.Parcelable
import com.beust.klaxon.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comparison(val left: String, val right: String, var score: Float = 9f, @Json(ignored = true) var read: Boolean = false) : Parcelable