package id.co.rumahcoding.sd

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.Helper.ExpertiseButton
import id.co.rumahcoding.sd.Helper.ExpertiseSwipeHelper
import id.co.rumahcoding.sd.Listener.ExpertiseButtonClickListener
import id.co.rumahcoding.sd.adapters.ExpertiseListAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.Expertise
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_expertise.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ExpertiseActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: ExpertiseListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expertise)

        app_bar.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        assign_expertise_button.setOnClickListener {
            val intent = Intent(this@ExpertiseActivity, AssignExpertiseActivity::class.java)
            startActivity(intent)
        }

        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val userId = sessionManager.fetchId()

        expertise_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        expertise_list.layoutManager = layoutManager

        val swipe = object: ExpertiseSwipeHelper(this, expertise_list, 200)
        {
            override fun instantiateExpertiseButton(
                viewHolder: RecyclerView.ViewHolder,
                buffer: MutableList<ExpertiseButton>
            ) {
                buffer.add(ExpertiseButton(this@ExpertiseActivity, "Delete", 30, 0, Color.parseColor("#FF3C30"),
                    object: ExpertiseButtonClickListener{
                        override fun onClick(pos: Int) {
                            Toast.makeText(this@ExpertiseActivity, "REMOVE EXPERTISE"+pos, Toast.LENGTH_SHORT).show()
                        }
                    }))
            }

        }

        fetchExpertise(token, userId)
    }

    private fun fetchExpertise(token: String?, userId: Int) {
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.expertiseData("Bearer ${token}", userId)

        call.enqueue(object : Callback<MutableList<Expertise>>{
            override fun onFailure(call: Call<MutableList<Expertise>>, t: Throwable) {
                Log.d("ERROR : ", t.message)
                Toast.makeText(applicationContext,"${t.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<MutableList<Expertise>>,
                response: Response<MutableList<Expertise>>
            ) {
                adapter = ExpertiseListAdapter(this@ExpertiseActivity, response.body() as MutableList<Expertise>)
                adapter.notifyDataSetChanged()
                expertise_list.adapter = adapter
            }

        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
