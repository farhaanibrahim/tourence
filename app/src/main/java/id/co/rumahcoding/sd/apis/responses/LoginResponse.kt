package id.co.rumahcoding.sd.apis.responses

class LoginResponse (val success: Boolean, val data: LoginData)

data class LoginData(
    val token: String
)