package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.LoginResponse
import id.co.rumahcoding.sd.apis.responses.SaveResponse
import id.co.rumahcoding.sd.apis.responses.UserDataResponse
import id.co.rumahcoding.sd.models.Role
import id.co.rumahcoding.sd.utils.PopupUtil
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_start.emailEditText
import kotlinx.android.synthetic.main.activity_start.passwordEditText
import kotlinx.android.synthetic.main.activity_start.registerButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val roleId = ArrayList<Int>()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val sessionManager = SessionManager(this)

//        apiEndPoint.roleData().enqueue(object: Callback<List<Role>>{
//            override fun onFailure(call: Call<List<Role>>, t: Throwable) {
//                Toast.makeText(this@RegisterActivity, t.message, Toast.LENGTH_LONG).show()
//            }
//
//            override fun onResponse(call: Call<List<Role>>, response: Response<List<Role>>) {
//                val saveResponse = response.body()
//                val role = arrayOfNulls<String>(response.body()!!.size)
//                for (i in 0 until response.body()!!.size){
//                    role[i] = saveResponse?.get(i)?.role_name
//                    roleId.add(saveResponse?.get(i)!!.id)
//                }
//
//                val arrayAdapter = ArrayAdapter(this@RegisterActivity, android.R.layout.simple_spinner_item, role)
//                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                role_spinner.adapter = arrayAdapter
//
//            }
//
//        })

        registerButton.setOnClickListener {
            PopupUtil.showLoading(this, null, "Please wait")

            val name = nameEditText.text.toString()
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            val phone = phoneEditText.text.toString()
//            val role_selected = role_spinner.selectedItemPosition
//            val role_id = roleId[role_selected]

            if(name.isBlank()) {
                nameEditText.error = "Name can't be empty"
                nameEditText.requestFocus()
                return@setOnClickListener
            }

            if(email.isBlank()) {
                emailEditText.error = "Email can't be empty"
                emailEditText.requestFocus()
                return@setOnClickListener
            }

            if(password.isBlank()) {
                passwordEditText.error = "Password can't be empty"
                passwordEditText.requestFocus()
                return@setOnClickListener
            }

            if(phone.isBlank()) {
                phoneEditText.error = "Phone can't be empty"
                phoneEditText.requestFocus()
                return@setOnClickListener
            }

//            if (role_id == 1) {
//                Snackbar.make(root_register_layout, "AVOID TO CHOOSE ROLE AS ADMIN!", Snackbar.LENGTH_LONG).show()
//                return@setOnClickListener
//            }

            //Save UserData Here
            apiEndPoint.registration(name,phone,email,password).enqueue(object: Callback<SaveResponse>{
                override fun onFailure(call: Call<SaveResponse>, t: Throwable) {
                    Toast.makeText(this@RegisterActivity, t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<SaveResponse>,
                    response: Response<SaveResponse>
                ) {
                    val saveResponse = response.body()
                    val success = saveResponse?.success
                    if (success == true) {
                        apiEndPoint.userLogin(email, password).enqueue(object: Callback<LoginResponse>{
                            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                                Toast.makeText(this@RegisterActivity, t.message, Toast.LENGTH_SHORT).show()
                            }

                            override fun onResponse(
                                call: Call<LoginResponse>,
                                response: Response<LoginResponse>
                            ) {
                                val saveResponse = response.body()
                                val success = saveResponse?.success

                                if(success == true){
                                    val token = saveResponse.data.token

                                    val callUserData = apiEndPoint.userData("Bearer "+token)
                                    callUserData.enqueue(object : Callback<UserDataResponse>{
                                        override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                                            val intent = Intent(this@RegisterActivity, StartActivity::class.java)
                                            startActivity(intent)
                                            finish()
                                        }

                                        override fun onResponse(
                                            call: Call<UserDataResponse>,
                                            response: Response<UserDataResponse>
                                        ) {
                                            val responseBody = response.body()
                                            val id = responseBody!!.data.user.id
                                            val name = responseBody.data.user.name
                                            val phone = responseBody.data.user.phone
                                            val email = responseBody.data.user.email
                                            val roleId = responseBody.data.user.role_id
                                            val expertise = responseBody.data.user.expertise

                                            sessionManager.saveAuthToken(token, id, name, phone, email, roleId, expertise.toString())

                                            val intent = Intent(this@RegisterActivity, HomeActivity::class.java)
                                            startActivity(intent)

                                            PopupUtil.dismissDialog()
                                            finish()
                                        }

                                    })

                                } else {
                                    btn_login.isEnabled = true
                                    PopupUtil.showMsg(this@RegisterActivity, "Wrong Email or Password", PopupUtil.SHORT)
                                    PopupUtil.dismissDialog()
                                }
                            }

                        })
                    } else {
                        PopupUtil.dismissDialog()
                        Toast.makeText(this@RegisterActivity, saveResponse?.message, Toast.LENGTH_LONG).show()
                    }
                }

            })
        }
    }

    fun back(view: View) {
        onBackPressed()
    }
    
    private fun initRole() {
        
    }
}
