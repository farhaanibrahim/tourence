package id.co.rumahcoding.sd

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.adapters.TravelAgentListAdapter
import kotlinx.android.synthetic.main.activity_select_city.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.TravelAgent
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: TravelAgentListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        val sessionManager = SessionManager(this.requireContext())

        view.user_name.text = sessionManager.fetchName()

        view.travel_agent_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this.requireContext())
        view.travel_agent_list.layoutManager = layoutManager

        fetchDataTravelAgent()

        // Inflate the layout for this fragment
        return view
    }

    private fun fetchDataTravelAgent() {
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.travelAgentData()
        call.enqueue(object: Callback<MutableList<TravelAgent>>{
            override fun onFailure(call: Call<MutableList<TravelAgent>>, t: Throwable) {
                Toast.makeText(context,t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<MutableList<TravelAgent>>,
                response: Response<MutableList<TravelAgent>>
            ) {
                adapter = TravelAgentListAdapter(this@HomeFragment.requireContext(), response.body() as MutableList<TravelAgent>)
                adapter.notifyDataSetChanged()
                travel_agent_list.adapter = adapter
            }
        })
    }
}
