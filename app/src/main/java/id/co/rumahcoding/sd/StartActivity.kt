package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.apis.responses.LoginResponse
import id.co.rumahcoding.sd.apis.responses.UserDataResponse
import id.co.rumahcoding.sd.utils.Connectivity
import id.co.rumahcoding.sd.utils.PopupUtil
import kotlinx.android.synthetic.main.activity_login.btn_login
import kotlinx.android.synthetic.main.activity_start.*
import retrofit2.*
import id.co.rumahcoding.sd.utils.SessionManager


class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        registerButton.setOnClickListener {
            val intent = Intent(this@StartActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        btn_login.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()

            if(email.isBlank()) {
                emailEditText.error = "Email can't be empty"
                emailEditText.requestFocus()
                return@setOnClickListener
            }

            if(password.isBlank()) {
                passwordEditText.error = "Password can't be empty"
                passwordEditText.requestFocus()
                return@setOnClickListener
            }

            if (Connectivity.isConnected(applicationContext)){
                login(email, password);
            } else {
                PopupUtil.showMsg(applicationContext, "Please check your internet connection", PopupUtil.SHORT)
            }
        }
    }

    private fun login(email: String, password: String) {
        val sessionManager = SessionManager(this)

        btn_login.isEnabled = false
        PopupUtil.showLoading(this, null, "Please wait")

        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.userLogin(email, password)

        call.enqueue(object : Callback<LoginResponse>{
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                val saveResponse = response.body()
                val success = saveResponse?.success

                if(success == true){
                    val token = saveResponse.data.token

                    val callUserData = apiEndPoint.userData("Bearer "+token)
                    callUserData.enqueue(object : Callback<UserDataResponse>{
                        override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                            val intent = Intent(this@StartActivity, StartActivity::class.java)
                            startActivity(intent)
                            finish()
                        }

                        override fun onResponse(
                            call: Call<UserDataResponse>,
                            response: Response<UserDataResponse>
                        ) {
                            val responseBody = response.body()
                            val id = responseBody!!.data.user.id
                            val name = responseBody.data.user.name
                            val phone = responseBody.data.user.phone
                            val email = responseBody.data.user.email
                            val roleId = responseBody.data.user.role_id
                            val expertise = responseBody.data.user.expertise

                            sessionManager.saveAuthToken(token, id, name, phone, email, roleId, expertise.toString())

                            val intent = Intent(this@StartActivity, HomeActivity::class.java)
                            startActivity(intent)
                            finish()

//                            if(roleId == "1"){
//                                val intent = Intent(this@StartActivity, HomeActivity::class.java)
//                                startActivity(intent)
//                                finish()
//                            } else if(roleId == "2"){
//                                val intent = Intent(this@StartActivity, AttractionActivity::class.java)
//                                startActivity(intent)
//                                finish()
//                            } else if(roleId == "3"){
//                                val intent = Intent(this@StartActivity, HomeActivity::class.java)
//                                startActivity(intent)
//                                finish()
//                            }
                        }

                    })

                } else {
                    btn_login.isEnabled = true
                    PopupUtil.showMsg(this@StartActivity, "Wrong Email or Password", PopupUtil.SHORT)
                    PopupUtil.dismissDialog()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                btn_login.isEnabled = true
                PopupUtil.showMsg(this@StartActivity, "Internal server error", PopupUtil.SHORT)
                PopupUtil.dismissDialog()
            }
        })
    }
}
