package id.co.rumahcoding.sd.utils

import android.util.Log

class ListUtils {
    companion object {
        fun combinations(list: List<String>): List<List<String>> {
            Log.d("ListUtils", list.toString())

            if(list.size <= 1) {
                return emptyList()
            }

            var combinations = mutableListOf<List<String>>()

            val first = list.first()

            for ((index, value) in list.withIndex()) {
                if(index > 0) {
                    combinations.add(listOf(first, value))
                }
            }

            //Log.d("ListUtils", "Last Index: ${list.lastIndex}")
            val subList = list.subList(1, list.size)
            val subCombinations = combinations(subList)

            if(subCombinations.isNotEmpty()) {
                combinations.addAll(subCombinations)
            }

            return combinations
        }
    }
}
