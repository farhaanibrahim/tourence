package id.co.rumahcoding.sd.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.co.rumahcoding.sd.R
import id.co.rumahcoding.sd.models.RecommendationElement
import kotlinx.android.synthetic.main.card_output_ahp_traveller.view.*

class OutputAHPAdapter(private val recommendationList: List<RecommendationElement>): RecyclerView.Adapter<OutputAHPAdapter.OutputAHPViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OutputAHPViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_output_ahp_traveller, parent, false)
        return OutputAHPViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return recommendationList.size
    }

    override fun onBindViewHolder(holder: OutputAHPViewHolder, position: Int) {
        val currentItem = recommendationList[position]
        holder.attractionName.text = currentItem.attraction
        holder.recommendationScore.text = currentItem.score
    }

    class OutputAHPViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val attractionName: TextView = itemView.attraction_name
        val recommendationScore: TextView = itemView.recommendation_score
    }

}