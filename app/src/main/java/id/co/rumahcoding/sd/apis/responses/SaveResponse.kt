package id.co.rumahcoding.sd.apis.responses

class SaveResponse (val success: Boolean, val message : String)