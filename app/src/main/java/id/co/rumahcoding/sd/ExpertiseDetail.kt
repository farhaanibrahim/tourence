package id.co.rumahcoding.sd

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import id.co.rumahcoding.sd.adapters.ExpertiseCityAdapter
import id.co.rumahcoding.sd.apis.ApiClient
import id.co.rumahcoding.sd.apis.ApiEndPoint
import id.co.rumahcoding.sd.models.CityExpertise
import id.co.rumahcoding.sd.models.Expertise
import id.co.rumahcoding.sd.utils.SessionManager
import kotlinx.android.synthetic.main.activity_expertise_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class ExpertiseDetail : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: ExpertiseCityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expertise_detail)

        app_bar.setOnClickListener {
            startActivity(Intent(this, ExpertiseActivity::class.java))
        }

        city_list.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        city_list.layoutManager = layoutManager

        val userExpertise = intent.getIntExtra("user_expertise_id", 0)
        val sessionManager = SessionManager(this)
        val token = sessionManager.fetchAuthToken()
        val apiEndPoint = ApiClient.getClient().create(ApiEndPoint::class.java)
        val call = apiEndPoint.expertiseDetail("Bearer ${token}", userExpertise)
        call.enqueue(object: Callback<Expertise>{
            override fun onFailure(call: Call<Expertise>, t: Throwable) {
                Log.d("ERROR : ", t.message!!)
                Toast.makeText(this@ExpertiseDetail, t.message , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Expertise>, response: Response<Expertise>) {
                val saveResponse = response.body()
                expertise_name.text = saveResponse!!.expertise.name
                expertise_email.text = saveResponse.expertise.email
                expertise_phone.text = saveResponse.expertise.phone

                adapter = ExpertiseCityAdapter(saveResponse.city_expertise as MutableList<CityExpertise>)
                city_list.adapter = adapter
            }

        })
    }
}
