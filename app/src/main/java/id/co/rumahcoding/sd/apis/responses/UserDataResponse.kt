package id.co.rumahcoding.sd.apis.responses

class UserDataResponse (val success: Boolean, val data: UserData)

data class UserData(val user: Data)

data class Data(val id: Int, val name: String, val phone: String, val email: String, val role_id: String, val expertise: Int)