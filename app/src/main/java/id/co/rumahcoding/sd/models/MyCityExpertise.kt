package id.co.rumahcoding.sd.models

data class MyCityExpertise (
    val id: Int,
    val userExpertiseID: Int,
    val cityID: Int,
    val createdAt: Any? = null,
    val updatedAt: Any? = null,
    val city: City
)