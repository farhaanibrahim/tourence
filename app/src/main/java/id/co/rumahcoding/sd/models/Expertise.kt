package id.co.rumahcoding.sd.models

import com.google.gson.annotations.SerializedName

class Expertise(
    val id: Int,
    val user_travel_agent: Int,
    val user_expertise: ExpertiseClass,
    val expertise: ExpertiseClass,
    val city_expertise: List<CityExpertise>
)

data class CityExpertise (
    val id: Int,
    val user_expertise_id: Int,
    val city_id: Int,
    val created_at: String? = null,
    val updated_at: String? = null,
    val city: MyCity
)

data class ExpertiseClass (
    val id: Int,
    val name: String,
    val phone: String,
    val email: String,
    val role_id: Int,
    val expertise: Int,
    val created_at: String,
    val updated_at: String
)